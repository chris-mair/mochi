/*

	MOCHI - MinOr CHess engIne

	(C) 2022-2024 Chris Mair <chris@1006.org>
	see LICENSE for license information

*/

package main

import (
	"fmt"
	"mochi/board"
	"mochi/engine"
	"mochi/mochilog"
	"os"
	"runtime/debug"
	"strconv"
)

func main() {

	banner := fmt.Sprintf("%s by %s (see LICENSE file)", engine.GetMOCHIName(), engine.GetMOCHIAuthor())
	mochilog.DebugLog(banner)

	// a cup of chamomile tea for the GC
	debug.SetGCPercent(800)

	board.InitLUTs()
	engine.InitLUTs()

	if len(os.Args) == 2 && (os.Args[1] == "-v" || os.Args[1] == "--version") {
		fmt.Println(engine.GetMOCHIDetailedName())
		return
	}

	fmt.Printf(banner)
	fmt.Println()

	if len(os.Args) == 1 {

		// no args
		// -> Go into UCI mode.

		engine.Parse()
		fmt.Println("bye")

	} else if len(os.Args) == 5 && os.Args[1] == "perft" && os.Args[2] == "fen" {

		// args: perft fen {fen} {maxDepth}
		// -> Perform a test computation of PERFT to depth maxDepth
		//    from the given position.

		var fen = os.Args[3]
		var maxDepth int
		var err error
		maxDepth, err = strconv.Atoi(os.Args[4])
		if err == nil && maxDepth >= 0 && maxDepth <= 20 {
			board.TestPosition(fen, maxDepth)
		} else {
			fmt.Printf("invalid depth (expected int between 0 and 20)\n")
		}

	} else if len(os.Args) == 4 && os.Args[1] == "checkmate" && os.Args[2] == "fen" {

		// args: checkmate fen {fen}
		// -> Analyze the position encoded by FEN {fen} until a checkmate
		//    is found (hit CTRL-C to interrupt).

		var fen = os.Args[3]
		var brd *board.Board
		var err error
		brd, err = board.FromFEN(fen)

		if err == nil {
			cc := make(chan string)
			engine.InfiniteSearch(cc, brd, true)
		} else {
			fmt.Printf("invalid fen (%s)\n", err)
		}

	} else {

		fmt.Printf("cannot understand args\n")

	}

}
