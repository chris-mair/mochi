/*

	MOCHI - MinOr CHess engIne

	(C) 2022-2024 Chris Mair <chris@1006.org>
	see LICENSE for license information

*/

package mochilog

import (
	"os"
)

var log *os.File

var enableDebugLog = false

func DebugLog(str string) {

	if !enableDebugLog {
		return
	}
	var err error
	if log == nil {
		log, err = os.CreateTemp("/tmp", "mochi-uci-debug-log*.txt")
		if err != nil {
			panic(err)
		}
	}
	_, err = log.WriteString(str + "\n")
	if err != nil {
		panic(err)
	}
	err = log.Sync()
	if err != nil {
		panic(err)
	}
}

func Panic(str string) {
	DebugLog("PANIC: " + str)
	panic(str)
}
