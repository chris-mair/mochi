/*

	MOCHI - MinOr CHess engIne

	(C) 2022-2024 Chris Mair <chris@1006.org>
	see LICENSE for license information

*/

package engine

import (
	"fmt"
	"runtime"
)

const version = "20250306_1100"

func GetMOCHIName() string {
	return fmt.Sprintf("MOCHI v.%s", version)
}

func GetMOCHIAuthor() string {
	return fmt.Sprintf("Chris Mair")
}

func GetMOCHIDetailedName() string {
	return fmt.Sprintf("MOCHI v.%s built with %s for %s-%s", version, runtime.Version(), runtime.GOOS, runtime.GOARCH)
}
