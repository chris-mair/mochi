/*

	MOCHI - MinOr CHess engIne

	(C) 2022-2024 Chris Mair <chris@1006.org>
	see LICENSE for license information

*/

package engine

import (
	"math/rand"
	"mochi/board"
)

type book struct {
	moveString string
	cumProb    float64
}

// Internal 1-move micro-book.

// When used without a book, MOCHI would generate the same game each time
// (earlier versions added a small random number to the evaluation - that is not
// the case anymore). To avoid this, MOCHI has an in-engine 1-move book.

// As white, MOCHI opens with a randomly selected move from the 10 most popular
// first moves from the Lichess master database (weighted according to the number
// of games they occurred in.)

// 1. (white)
// move  game count  probability        cumulative probability
// e2e4     1231250  0.453553204876015  0.453553204876015
// d2d4      977040  0.359910353942791  0.813463558818806
// g1f3      274989  0.101297171375148  0.914760730193953
// c2c4      187790  0.069175842715668  0.983936572909622
// g2g3       19809  0.007297003399301  0.991233576308922
// b2b3       10718  0.003948169136943  0.995181745445865
// f2f4        6395  0.002355713904716  0.997537459350582
// b1c3        3754  0.001382853791760  0.998920313142342
// b2b4        2089  0.000769520929938  0.999689834072280
// e2e3         842  0.000310165927720  1.000000000000000

var book1 = [...]book{
	{"e2e4", 0.453553204876015},
	{"d2d4", 0.813463558818806},
	{"g1f3", 0.914760730193953},
	{"c2c4", 0.983936572909622},
	{"g2g3", 0.991233576308922},
	{"b2b3", 0.995181745445865},
	{"f2f4", 0.997537459350582},
	{"b1c3", 0.998920313142342},
	{"b2b4", 0.999689834072280},
	{"e2e3", 1.000000000000000}}

// In the same way, after 1. e4, 1. d4, 1. Nf3 or 1. c4 MOCHI as black
// randomly selects a move from the 5 most popular answers.

// 1. e4  (black)
// move  game count  probability        cumulative probability
// c7c5      567659  0.496175492911704  0.496175492911704
// e7e5      288202  0.251909631324684  0.748085124236388
// e7e6      146105  0.127706458264318  0.875791582500706
// c7c6       98794  0.086353183243318  0.962144765744024
// d7d6       43309  0.037855234255976  1.000000000000000

var book2_e4 = [...]book{
	{"c7c5", 0.496175492911704},
	{"e7e5", 0.748085124236388},
	{"e7e6", 0.875791582500706},
	{"c7c6", 0.962144765744024},
	{"d7d6", 1.000000000000000}}

// 1. d4  (black)
// move  game count  probability        cumulative probability
// g8f6  	 593543  0.632025786061726  0.632025786061726
// d7d5  	 250178  0.266398470044042  0.898424256105768
// e7e6  	  40257  0.042867091465129  0.941291347570897
// f7f5  	  27573  0.029360715228854  0.970652062799751
// d7d6  	  27561  0.029347937200249  1.000000000000000

var book2_d4 = [...]book{
	{"g8f6", 0.632025786061726},
	{"d7d5", 0.898424256105768},
	{"e7e6", 0.941291347570897},
	{"f7f5", 0.970652062799751},
	{"d7d6", 1.000000000000000}}

// 1. Nf3 (black)
// move  game count  probability        cumulative probability
// g8f6      128272  0.491844262609376  0.491844262609376
// d7d5       80615  0.309108965559552  0.800953228168928
// c7c5       32053  0.122903549873849  0.923856778042777
// g7g6       13151  0.050426000199388  0.974282778242165
// f7f5        6707  0.025717221757836  1.000000000000000

var book2_nf3 = [...]book{
	{"g8f6", 0.491844262609376},
	{"d7d5", 0.800953228168928},
	{"c7c5", 0.923856778042777},
	{"g7g6", 0.974282778242165},
	{"f7f5", 1.000000000000000}}

// 1. c4  (black)
// move  game count  probability        cumulative probability
// g8f6       52587  0.321587788873736  0.321587788873736
// e7e5       46196  0.282504601799135  0.604092390672872
// e7e6       28181  0.172336613198143  0.776429003871015
// c7c5       21212  0.129718755159825  0.906147759030840
// c7c6       15347  0.093852240969160  1.000000000000000

var book2_c4 = [...]book{
	{"g8f6", 0.321587788873736},
	{"e7e5", 0.604092390672872},
	{"e7e6", 0.776429003871015},
	{"c7c5", 0.906147759030840},
	{"c7c6", 1.000000000000000}}

func getBookMove(brd *board.Board) string {

	currentHash := board.ComputeHash(brd)
	var selectedBook []book
	var bookMove = ""

	switch currentHash {
	case 0x64a0b4b1587554a5: // starting position, white to move
		selectedBook = book1[:]
	case 0x9ac903060cab3272: // 1. e4, black to move
		selectedBook = book2_e4[:]
	case 0x8479a77ccf58a49b: // 1. d4, black to move
		selectedBook = book2_d4[:]
	case 0x99d5fc966309ef89: // 1. Nf3, black to move
		selectedBook = book2_nf3[:]
	case 0x80172ad3d9776357: // 1. c4, black to move
		selectedBook = book2_c4[:]
	default:
		return ""
	}

	r := rand.Float64()
	for j := 0; j < len(selectedBook); j++ {
		if r <= selectedBook[j].cumProb {
			bookMove = selectedBook[j].moveString
			break
		}
	}

	return bookMove

}
