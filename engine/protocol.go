/*

	MOCHI - MinOr CHess engIne

	(C) 2022-2024 Chris Mair <chris@1006.org>
	see LICENSE for license information

*/

package engine

import (
	"bufio"
	"fmt"
	"mochi/board"
	"mochi/mochilog"
	"os"
	"regexp"
	"strconv"
	"strings"
)

// Parse UCI protocol command line.
// UCI reference:
//	https://en.wikipedia.org/wiki/Universal_Chess_Interface
//	https://www.shredderchess.com/download/div/uci.zip

func Parse() {

	tmpbrd := board.FromStartingPosition()

	spaceRE := regexp.MustCompile(`\s+`)
	input := bufio.NewScanner(os.Stdin)

	cc := make(chan string)

	for {

		// get the UCI line and break it into tokens

		if !input.Scan() {
			break
		}
		line := strings.TrimSpace(spaceRE.ReplaceAllLiteralString(input.Text(), " "))
		if line == "" {
			continue
		}
		tokens := strings.Split(line, " ")
		if len(tokens) == 0 {
			continue
		}

		mochilog.DebugLog("UCI IN:  " + line)

		// parse tokens

		for len(tokens) > 0 {

			switch tokens[0] {

			case "register":
				// ignored

			case "uci":
				PrintAndLog(fmt.Sprintf("id name %s", GetMOCHIName()))
				PrintAndLog(fmt.Sprintf("id author %s", GetMOCHIAuthor()))
				PrintAndLog("")
				PrintAndLog("option name Dummy type spin default 4 min 1 max 64")
				PrintAndLog("uciok")

			case "isready":
				PrintAndLog("readyok")

			case "quit":
				return

			case "setoption":
				// setoption name Dummy value %d
				setoptionDone := false
				if len(tokens) >= 5 {
					if tokens[1] == "name" && strings.EqualFold(tokens[2], "Dummy") && tokens[3] == "value" {
						t, err := strconv.Atoi(tokens[4])
						if err == nil && t >= 1 && t <= 64 {
							setoptionDone = true
						}
					}
				}
				if !setoptionDone {
					mochilog.DebugLog("could not parse setoption line")
				}

			case "ucinewgame":
				// no answer needed

			case "position":
				if len(tokens) >= 2 {
					switch tokens[1] {
					case "startpos":
						// examples:
						//   position startpos
						//   position startpos moves d2d4 d7d5 c2c4
						mochilog.DebugLog("setting up position startpos")
						ClearPositions()
						tmpbrd = board.FromStartingPosition()
						AddPosition(tmpbrd)
						if len(tokens) > 2 && tokens[2] == "moves" {
							for i := 3; i < len(tokens); i++ {
								err := board.ApplyMove(tmpbrd, tokens[i])
								if err == nil {
									mochilog.DebugLog("  move " + tokens[i])
									AddPosition(tmpbrd)
								} else {
									mochilog.DebugLog("  move " + tokens[i] + " INVALID, ignored")
								}
							}
						}
						mochilog.DebugLog(board.ToString(tmpbrd))
					case "fen":
						if len(tokens) > 2 {
							// example:
							//   position fen rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
							//   position fen rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1 moves d2d4 d7d5 c2c4
							mochilog.DebugLog("setting up position fen")
							ClearPositions()
							fenstr := strings.Join(tokens[2:], " ")
							movespos := strings.Index(fenstr, "moves ")
							moves := ""
							if movespos > 0 {
								moves = fenstr[movespos+6:]
								fenstr = fenstr[0 : movespos-1]
							}
							var err error
							tmpbrd, err = board.FromFEN(fenstr)
							if err != nil {
								mochilog.DebugLog(fmt.Sprintf("fen invalid (%s), using startpos instead", err))
								tmpbrd = board.FromStartingPosition()
							}
							AddPosition(tmpbrd)
							if moves != "" {
								moveslist := strings.Split(moves, " ")
								for i := 0; i < len(moveslist); i++ {
									err := board.ApplyMove(tmpbrd, moveslist[i])
									if err == nil {
										mochilog.DebugLog("  move " + moveslist[i])
										AddPosition(tmpbrd)
									} else {
										mochilog.DebugLog("  move " + moveslist[i] + " INVALID, ignored")
									}
								}
							}
							mochilog.DebugLog(board.ToString(tmpbrd))
						}
					}
				}

			case "go":
				// for now, we support just infinite analysis or game mode
				// TODO: extend support for other go command variations
				if len(tokens) >= 2 && tokens[1] == "infinite" {
					// infinite analysis
					go InfiniteSearch(cc, tmpbrd, false)
				} else {
					// game mode
					var wtime float32 = 60000.0
					var btime float32 = 60000.0
					var winc float32 = 0.0
					var binc float32 = 0.0
					var x float64
					var err error
					for pos := 1; ; pos += 2 {
						if len(tokens) < pos+1 {
							break
						}
						switch tokens[pos] {
						case "wtime":
							x, err = strconv.ParseFloat(tokens[pos+1], 32)
							if err == nil {
								wtime = float32(x)
							}
						case "btime":
							x, err = strconv.ParseFloat(tokens[pos+1], 32)
							if err == nil {
								btime = float32(x)
							}
						case "winc":
							x, err = strconv.ParseFloat(tokens[pos+1], 32)
							if err == nil {
								winc = float32(x)
							}
						case "binc":
							x, err = strconv.ParseFloat(tokens[pos+1], 32)
							if err == nil {
								binc = float32(x)
							}
						}
					}
					go Search(cc, tmpbrd, wtime, btime, winc, binc)
				}

			case "stop":
				// TODO: handle deadlock if we get a stop while we do not compute anything
				cc <- "stop"

			default:
				// UCI requires the engine to parse the rest of the line if a command is unknown,
				// so we pop the first token and continue the parse loop
				mochilog.DebugLog(fmt.Sprintf("popping unknown token '%s'\n", tokens[0]))
				tokens = tokens[1:]
				continue
			}

			// command identified: break out
			break

		}

	}
	return
}

func PrintAndLog(str string) {
	fmt.Println(str)
	mochilog.DebugLog("UCI OUT: " + str)
}
