/*

	MOCHI - MinOr CHess engIne

	(C) 2022-2024 Chris Mair <chris@1006.org>
	see LICENSE for license information

*/

package engine

import (
	"fmt"
	"math"
	"mochi/board"
	"mochi/mochilog"
	"slices"
	"time"
)

// After a "go infinite" command, MOCHI searches until it receives an UCI
// stop command. The search depth is increased starting from some low value.
// After each depth step done, an UCI info string is send. After the stop,
// a final info and "bestmove" string is send.
//
// There is also a mate-in-N puzzle mode. If checkmatePuzzle == true, analysis
// will return as soon as a checkmate is found.

func InfiniteSearch(uciChan chan string, brd *board.Board, checkmatePuzzle bool) {

	mochilog.DebugLog("go infinite: entry")

	var baseDepth = 2
	var extendedDepth = baseDepth + 4

	atLeastOneSuccessfulSearch := false

	var duration float32 = 0.0
	var bestMove = board.NaMInt
	var bestEval float32
	var nodeCount uint64
	var baseDepthDone int
	var extendedDepthDone int

	iterationsReady := false

	for { // incrementing depth

		// Start the search using alpha-beta pruning in a Goroutine.

		mochilog.DebugLog(fmt.Sprintf("go infinite: starting search at depth %d-%d", baseDepth, extendedDepth))
		t0 := time.Now()
		signalStop = false
		workerChan := make(chan walkResult)
		go abwalk(brd, 0, baseDepth, extendedDepth, -math.MaxFloat32, math.MaxFloat32, bestMove, workerChan)

		// Listen to the workerChan channel (for the search result) and to
		// the UCI channel (for a stop command); do this in a non-blocking way,
		// so we're able to react quickly.

		workerReady := false

		for !iterationsReady && !workerReady { // spin loop

			select {
			case msg := <-workerChan:
				atLeastOneSuccessfulSearch = true
				duration = float32(time.Since(t0)) / 1.0e9
				bestMove = msg.move
				bestEval = msg.eval
				nodeCount = msg.cnt
				baseDepthDone = baseDepth
				extendedDepthDone = extendedDepth
				workerReady = true
				mochilog.DebugLog(fmt.Sprintf("go infinite: search ready (%d nodes in %.3fs, %.0f knodes/s)", nodeCount, duration, float32(nodeCount)/1.0e3/duration))
			default:
				// do not block
			}
			select {
			case msg := <-uciChan:
				if msg == "stop" {
					mochilog.DebugLog("go infinite: received UCI stop command, worker signaled, stop")
					signalStop = true
					iterationsReady = true
				}
			default:
				// do not block
			}
			time.Sleep(2.0 * time.Millisecond)

		} // end spin loop

		if !atLeastOneSuccessfulSearch {

			// We exited the spin loop before at least one search was finished.
			// Though this is extremely unlikely, we better catch this.
			// We'll just return the first valid move (if any).

			boards := board.GetValidMoves(brd)
			if len(boards) > 0 {
				mochilog.DebugLog("go infinite: no search completed, will return first valid move")
				PrintAndLog(fmt.Sprintf("info depth 1 seldepth 1 multipv 1 score cp %.0f nodes 1 nps 0 pv %s",
					board.GetEvaluation(&boards[0]),
					board.MoveToString(board.GetLastMove(&boards[0]))))
				PrintAndLog(fmt.Sprintf("bestmove %s",
					board.MoveToString(board.GetLastMove(&boards[0]))))
			} else {
				mochilog.DebugLog("go infinite: no search completed and no valid move, I don't know what to do")
			}
			return
		}

		if bestEval >= -999999 && bestEval <= 999999 {

			// No checkmate seen => UCI wants a centipawn (CP) value.
			// CPs in UCI are understood from the engines point of view,
			// so for black, a negative eval means a positive CP value.

			var centiPawns float32
			if board.GetSideToMove(brd) == board.WHITE {
				centiPawns = bestEval
			} else {
				centiPawns = -bestEval
			}
			infoStr := "info depth %d seldepth %d multipv 1 score cp %.0f nodes %d nps %.0f pv %s"
			PrintAndLog(fmt.Sprintf(infoStr, baseDepthDone, extendedDepthDone, centiPawns, nodeCount, float64(nodeCount)/float64(duration), board.MoveToString(bestMove)))

		} else {

			// Checkmate seen => UCI wants a "mate in N" indication.
			// The depth is encoded in the eval. UCI wants to see the number
			// of moves (not plies) to mate. The number should be negative if
			// the engine is checkmated.

			var mateInPlies float32
			mateInPlies = float32(100.0 - math.Abs(float64(bestEval/1.0e6)))
			var mateInMoves int
			mateInMoves = int((mateInPlies + 1) / 2)
			if (board.GetSideToMove(brd) == board.WHITE && bestEval < 0) ||
				(board.GetSideToMove(brd) == board.BLACK && bestEval > 0) {
				mateInMoves = -mateInMoves
			}
			infoStr := "info depth %d seldepth %d multipv 1 score mate %d nodes %d nps %.0f pv %s"
			PrintAndLog(fmt.Sprintf(infoStr, baseDepthDone, extendedDepthDone, mateInMoves, nodeCount, float64(nodeCount)/float64(duration), board.MoveToString(bestMove)))

			// Mate-in-N puzzle mode? We don't need to iterate further.
			// Otherwise, we honour the "go infinite" command and iterate
			// further, even though we won't find a better move.

			if checkmatePuzzle {
				iterationsReady = true
			}
		}

		if iterationsReady {
			PrintAndLog(fmt.Sprintf("bestmove %s", board.MoveToString(bestMove)))
			mochilog.DebugLog("go infinite: exit")
			return
		}

		baseDepth++
		extendedDepth++
	} // end incrementing depth

}

// After a "go" command, MOCHI searches until it receives an UCI
// stop command or the maximum allocated search time is reached
// (or close to be reached).
// The search depth is increased starting from some low value.
// After each depth step done, an UCI info string is send. After
// the search ends, a "bestmove" string is send.

func Search(uciChan chan string, brd *board.Board, wtime, btime, winc, binc float32) {

	// TODO: reduce duplicated code between this function and InfiniteSearch()

	mochilog.DebugLog(fmt.Sprintf("go: entry (wtime = %.0f, btime = %.0f, winc = %.0f, binc = %.0f)", wtime, btime, winc, binc))

	bookMove := getBookMove(brd)
	if bookMove != "" {
		mochilog.DebugLog(fmt.Sprintf("go: found book move %s", bookMove))
		tmpboard := brd
		err := board.ApplyMove(brd, bookMove)
		if err == nil {
			PrintAndLog(fmt.Sprintf("info depth 1 seldepth 1 multipv 1 score cp %.0f nodes 1 nps 0 pv %s", board.GetEvaluation(tmpboard), bookMove))
			PrintAndLog(fmt.Sprintf("bestmove %s", bookMove))
			AddPosition(tmpboard)
			return
		}
		mochilog.DebugLog("go: book move invalid (?), ignored") // should never happen
	}

	// Naively assume tha game will go on for expectedMoveNum more moves
	// and hence we will use at most 1/expectedMoveNum of expected remaining time
	// (minus a safety margin of 10 seconds) to compute the best move.
	// The so computed maximum move time is capped to values between 0.1 s and 20 s.

	tStart := time.Now()

	const expectedMoveNum float32 = 20.0
	var maxMoveTime float32
	if board.GetSideToMove(brd) == board.WHITE {
		maxMoveTime = (wtime + expectedMoveNum*winc - 10000) / expectedMoveNum
	} else {
		maxMoveTime = (btime + expectedMoveNum*binc - 10000) / expectedMoveNum
	}
	if maxMoveTime < 100.0 {
		maxMoveTime = 100.0
	}
	if maxMoveTime > 20000.0 {
		maxMoveTime = 20000.0
	}
	mochilog.DebugLog(fmt.Sprintf("go: max move time = %.0f", maxMoveTime))

	var baseDepth = 2
	var extendedDepth = baseDepth + 4

	atLeastOneSuccessfulSearch := false

	var duration float32 = 0.0
	var bestMove = board.NaMInt
	var bestEval float32
	var nodeCount uint64
	var baseDepthDone int
	var extendedDepthDone int

	iterationsReady := false

	for { // incrementing depth

		// Start the search using alpha-beta pruning in a Goroutine.

		mochilog.DebugLog(fmt.Sprintf("go: starting search at depth %d-%d", baseDepth, extendedDepth))
		t0 := time.Now()
		signalStop = false
		workerChan := make(chan walkResult)
		go abwalk(brd, 0, baseDepth, extendedDepth, -math.MaxFloat32, math.MaxFloat32, bestMove, workerChan)

		// Listen to the workerChan channel (for the search result) and to
		// the UCI channel (for a stop command); do this in a non-blocking way,
		// so we're able to react quickly.

		workerReady := false

		for !iterationsReady && !workerReady { // spin loop

			select {
			case msg := <-workerChan:
				atLeastOneSuccessfulSearch = true
				duration = float32(time.Since(t0)) / 1.0e9
				bestMove = msg.move
				bestEval = msg.eval
				nodeCount = msg.cnt
				baseDepthDone = baseDepth
				extendedDepthDone = extendedDepth
				workerReady = true
				mochilog.DebugLog(fmt.Sprintf("go: search ready (%d nodes in %.3fs, %.0f knodes/s)", nodeCount, duration, float32(nodeCount)/1.0e3/duration))

				// If we're less than `duration` away from the timeout, we likely
				// won't manage to do another iteration, so we stop here instead
				// of wasting time.

				if float32(time.Since(tStart))/1.0e6 > maxMoveTime-duration*1000.0 {
					mochilog.DebugLog("go: next depth iteration would likely exceed maximum move time, stop")
					iterationsReady = true
				}

			default: // do not block
			}
			select {
			case msg := <-uciChan:
				if msg == "stop" {
					mochilog.DebugLog("go: received UCI stop command, worker signaled, stop")
					signalStop = true
					iterationsReady = true
				}
			default: // do not block
			}

			// Are we out of time?

			if float32(time.Since(tStart))/1.0e6 > maxMoveTime {
				mochilog.DebugLog("go: maximum move time reached, worker signaled, stop")
				signalStop = true
				iterationsReady = true
			}
			time.Sleep(2.0 * time.Millisecond)

		} // end spin loop

		if !atLeastOneSuccessfulSearch {

			// We exited the spin loop before at least one search was finished.
			// Though this is extremely unlikely, we better catch this.
			// We'll just return the first valid move (if any).

			boards := board.GetValidMoves(brd)
			if len(boards) > 0 {
				mochilog.DebugLog("go: no search completed, will return first valid move")
				PrintAndLog(fmt.Sprintf("info depth 1 seldepth 1 multipv 1 score cp %.0f nodes 1 nps 0 pv %s",
					board.GetEvaluation(&boards[0]),
					board.MoveToString(board.GetLastMove(&boards[0]))))
				PrintAndLog(fmt.Sprintf("bestmove %s",
					board.MoveToString(board.GetLastMove(&boards[0]))))
				AddPosition(&boards[0])
			} else {
				mochilog.DebugLog("go: no search completed and no valid move, I don't know what to do")
			}
			return
		}

		if bestEval >= -999999 && bestEval <= 999999 {

			// No checkmate seen => UCI wants a centipawn (CP) value.
			// CPs in UCI are understood from the engines point of view,
			// so for black, a negative eval means a positive CP value.

			var centiPawns float32
			if board.GetSideToMove(brd) == board.WHITE {
				centiPawns = bestEval
			} else {
				centiPawns = -bestEval
			}
			infoStr := "info depth %d seldepth %d multipv 1 score cp %.0f nodes %d nps %.0f pv %s"
			PrintAndLog(fmt.Sprintf(infoStr, baseDepthDone, extendedDepthDone, centiPawns, nodeCount, float64(nodeCount)/float64(duration), board.MoveToString(bestMove)))

		} else {

			// Checkmate seen => UCI wants a "mate in N" indication.
			// The depth is encoded in the eval. UCI wants to see the number
			// of moves (not plies) to mate. The number should be negative if
			// the engine is checkmated.

			var mateInPlies float32
			mateInPlies = float32(100.0 - math.Abs(float64(bestEval/1.0e6)))
			var mateInMoves int
			mateInMoves = int((mateInPlies + 1) / 2)
			if (board.GetSideToMove(brd) == board.WHITE && bestEval < 0) ||
				(board.GetSideToMove(brd) == board.BLACK && bestEval > 0) {
				mateInMoves = -mateInMoves
			}
			infoStr := "info depth %d seldepth %d multipv 1 score mate %d nodes %d nps %.0f pv %s"
			PrintAndLog(fmt.Sprintf(infoStr, baseDepthDone, extendedDepthDone, mateInMoves, nodeCount, float64(nodeCount)/float64(duration), board.MoveToString(bestMove)))

			// Since we have a checkmate, we don't need to iterate further.

			iterationsReady = true
		}

		if iterationsReady {
			PrintAndLog(fmt.Sprintf("bestmove %s", board.MoveToString(bestMove)))
			tmpboard := brd
			err := board.ApplyMove(brd, board.MoveToString(bestMove))
			if err == nil {
				AddPosition(tmpboard)
			} else {
				// should never be reached
				mochilog.Panic("cannot apply the move I found")
			}
			mochilog.DebugLog("go: exit")
			return
		}

		baseDepth++
		extendedDepth++

	} // end incrementing depth

}

// The hashes of the positions reached in the current game so far.

var positions []uint64 = nil

func ClearPositions() {
	positions = nil
}

func AddPosition(brd *board.Board) {
	positions = append(positions, board.ComputeHash(brd))
}

// Recursive tree traversal using alpha-beta pruning ( https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning ).
// Note:
// - alpha is the minimum score that the maximizing player is assured of,
// - beta is the maximum score that the minimizing player is assured of.

var signalStop = false

type walkResult struct {
	move uint16
	eval float32
	cnt  uint64
}

type boardRef struct {
	Index int
	Eval  float32
}

func abwalk(brd *board.Board, depth int, baseDepth int, extendedDepth int, alpha float32, beta float32, VIPMove uint16, rch chan walkResult) (float32, uint64) {

	// return evaluation when the cutoff depth is reached
	if depth >= baseDepth {
		if board.GetLastMoveKind(brd) == board.MOVETYPE_QUIET || depth >= extendedDepth {
			e := board.GetEvaluation(brd)
			return e, 1
		}
	}

	// quickly exit when signaled
	if signalStop {
		return 0.0, 0
	}

	var value float32

	// get possible moves
	boards := board.GetValidMoves(brd)

	// if there are no moves, it's a stalemate or checkmate
	if len(boards) == 0 {
		if board.GetSideToMove(brd) == board.WHITE {
			r, f := board.FindKing(brd, board.WHITE)
			if !board.IsAttacked(brd, board.WHITE, r, f) {
				// white is stalemate
				value = 0.0
			} else {
				// white is checkmate, the lower depth the better
				value = -100000000.0 + float32(depth)*1000000.0
			}
		} else {
			r, f := board.FindKing(brd, board.BLACK)
			if !board.IsAttacked(brd, board.BLACK, r, f) {
				// black is stalemate
				value = 0.0
			} else {
				// black is checkmate, the lower depth the better
				value = 100000000.0 - float32(depth)*1000000.0
			}
		}
		if rch != nil {
			rch <- walkResult{board.GetLastMove(brd), value, 1}
		}
		return value, 1
	}

	// Perform move ordering in order to optimize alpha-beta pruning.
	// For the nodes closest to the root, do move ordering according
	// to the evaluation. Farther away just order by move kind. For
	// the farthest nodes, the overhead is not worth it.

	var lut []boardRef

	if depth <= baseDepth-3 {
		// sort by eval
		lut = make([]boardRef, len(boards))
		for i := 0; i < len(boards); i++ {
			lut[i].Index = i
			lut[i].Eval = board.GetEvaluation(&boards[i])
		}
		if board.GetSideToMove(brd) == board.WHITE {
			slices.SortFunc(lut, func(a, b boardRef) int {
				if a.Eval > b.Eval {
					return -1
				} else if a.Eval == b.Eval {
					return 0
				} else {
					return 1
				}
			})
		} else {
			slices.SortFunc(lut, func(a, b boardRef) int {
				if a.Eval < b.Eval {
					return -1
				} else if a.Eval == b.Eval {
					return 0
				} else {
					return 1
				}
			})
		}
	} else if depth <= baseDepth-2 {
		// sort by move kind
		lut = make([]boardRef, len(boards))
		for i := 0; i < len(boards); i++ {
			lut[i].Index = i
			switch board.GetLastMoveKind(&boards[i]) {
			case board.MOVETYPE_CHECK_WITH_CAPTURE:
				lut[i].Eval = 3.0
			case board.MOVETYPE_CHECK:
				lut[i].Eval = 2.0
			case board.MOVETYPE_CAPTURE:
				lut[i].Eval = 1.0
			default:
				lut[i].Eval = 0.0
			}
		}
		slices.SortFunc(lut, func(a, b boardRef) int {
			if a.Eval > b.Eval {
				return -1
			} else if a.Eval == b.Eval {
				return 0
			} else {
				return 1
			}
		})
	} else {
		// no sorting, just use identity LUT
		lut = eye
	}

	// For depth 0, if VIPMove is given, sort that one to the top.
	// The idea is that when we increase depth, we use the best move
	// from the search at the previous iteration (VIPMove) as the first
	// move to search in the next iteration.

	if depth == 0 && VIPMove != board.NaMInt {
		VIPIndex := -1
		for i := 0; i < len(boards); i++ {
			if board.GetLastMove(&boards[i]) == VIPMove {
				VIPIndex = i
				mochilog.DebugLog(fmt.Sprintf("    abwalk: move %s sorted to top", board.MoveToString(VIPMove)))
				break
			}
		}
		if VIPIndex >= 0 {
			for i := 1; i < len(lut); i++ {
				if lut[i].Index == VIPIndex {
					tmp := lut[0]
					lut[0] = lut[i]
					lut[i] = tmp
					break
				}
			}
		}
		if board.GetLastMove(&boards[lut[0].Index]) != VIPMove {
			panic("")
		}
	}

	// Handling of three-fold repetitions.
	// For depth 0, we check if a candidate move would lead to a three-fold repetition
	// (the same position and state appears a third time on the board). In that case
	// we force the eval to 0.0 as this move would immediately lead to a draw.
	// It would be better to check for three-fold repetition along the whole search
	// tree, but computing all the board hashes is expensive (as we do not yet have
	// code for incremental hashing).

	if board.GetSideToMove(brd) == board.WHITE {
		// maximizing player
		value = -math.MaxFloat32
		var aggcnt uint64 = 0
		var best_i = -1
		var e float32
		var c uint64
		for i := 0; i < len(boards); i++ {
			if depth == 0 && isThreeFold(board.ComputeHash(&boards[lut[i].Index])) {
				mochilog.DebugLog(fmt.Sprintf("    abwalk: move %s would be a three-fold repetion, forcing eval to 0.0", board.MoveToString(board.GetLastMove(&boards[lut[i].Index]))))
				e, c = 0.0, 1
			} else {
				e, c = abwalk(&boards[lut[i].Index], depth+1, baseDepth, extendedDepth, alpha, beta, board.NaMInt, nil)
			}
			aggcnt += c
			if e > value {
				best_i = i
			}
			value = max(value, e)
			alpha = max(alpha, value)
			if value >= beta {
				break // beta cutoff
			}
		}
		if rch != nil {
			rch <- walkResult{board.GetLastMove(&boards[lut[best_i].Index]), value, aggcnt}
		}
		return value, aggcnt
	} else {
		// minimizing player
		value = math.MaxFloat32
		var aggcnt uint64 = 0
		var best_i = -1
		var e float32
		var c uint64
		for i := 0; i < len(boards); i++ {
			if depth == 0 && isThreeFold(board.ComputeHash(&boards[lut[i].Index])) {
				mochilog.DebugLog(fmt.Sprintf("    abwalk: move %s would be a three-fold repetion, forcing eval to 0.0", board.MoveToString(board.GetLastMove(&boards[lut[i].Index]))))
				e, c = 0.0, 1
			} else {
				e, c = abwalk(&boards[lut[i].Index], depth+1, baseDepth, extendedDepth, alpha, beta, board.NaMInt, nil)
			}
			aggcnt += c
			if e < value {
				best_i = i
			}
			value = min(value, e)
			beta = min(beta, value)
			if value <= alpha {
				break // alpha cutoff
			}
		}
		if rch != nil {
			rch <- walkResult{board.GetLastMove(&boards[lut[best_i].Index]), value, aggcnt}
		}
		return value, aggcnt
	}
}

func isThreeFold(hash uint64) bool {
	repeated := 0
	for j := 0; j < len(positions); j++ {
		if positions[j] == hash {
			repeated++
			if repeated >= 2 {
				return true
			}
		}
	}
	return false
}

// identity LUT

var eye []boardRef

func InitLUTs() {
	eye = make([]boardRef, 500)
	for i := 0; i < len(eye); i++ {
		eye[i].Index = i
	}
}
