/*

	MOCHI - MinOr CHess engIne

	(C) 2022-2024 Chris Mair <chris@1006.org>
	see LICENSE for license information

*/

package board

// Bit field describing the castling rights.

type CastlingRightType uint8

const (
	WQS CastlingRightType = 1 << iota
	WKS
	BQS
	BKS
)

// Bit field describing which side has already castled.

type CastlingDoneType uint8

const (
	WHITE_CASTLED CastlingDoneType = 1 << iota
	BLACK_CASTLED
)

// SideType is just WHITE or BLACK.

type SideType uint8

const (
	WHITE SideType = iota
	BLACK
)

// Type of last move (note the order is significant for move ordering in abwalk()).

type LastMoveType uint8

const (
	MOVETYPE_UNKNOWN LastMoveType = iota
	MOVETYPE_QUIET
	MOVETYPE_CAPTURE
	MOVETYPE_CHECK
	MOVETYPE_CHECK_WITH_CAPTURE
)

// Board type holds the complete board state.

type Board struct {
	grid                [8][8]byte        // [rank][file], value = 0 or ASCII piece char (FEN)
	moveNumber          uint16            // full move number, starting at 1
	lastMove            uint16            // last move encoded as a bit field (see below)
	castlingRights      CastlingRightType // bit field: WQS | WKS | BQS | BKS
	castlingDone        CastlingDoneType  // bit field: WHITE_CASTLED | BLACK_CASTLED
	toMove              SideType          // WHITE or BLACK
	enPassantTargetRank int8              // 0-7 or -1 (not set)
	enPassantTargetFile int8              // idem
	lastMoveKind        LastMoveType      // move classification
	halfMoveClock       uint8             // half-moves since last capture or pawn move
}

// lastMove is a bit field:
// bits 0,1,2    -> to file
// bits 3,4,5    -> to rank
// bits 6,7,8    -> from file
// bits 9,10,11  -> from rank
// bits 12,13,14 -> piece to promote to (0 -> none, 1->q, 2->r, 3->b, 4->n).
// A value of 0xffff indicated the absence of the information about the last move.
// The bit field is converted to and from a string representation (long algebraic notation,
// e.g. "e2e4") only where needed for human-readable output or the UCI protocol.

// Constants used to indicate a move is not set ("Not a Move").

const NaMInt uint16 = 0xffff
const NaMString string = ""
