/*

	MOCHI - MinOr CHess engIne

	(C) 2022-2024 Chris Mair <chris@1006.org>
	see LICENSE for license information

*/

package board

import (
	"fmt"
	"time"
)

// PERFT - count leave nodes in the move generation tree up to given depth
// for the given position

func TestPosition(fen string, maxDepth int) {

	t0 := time.Now()
	var posCount uint64 = 0
	var brd *Board
	var err error

	brd, err = FromFEN(fen)
	if err != nil {
		fmt.Printf("%s", err)
		return
	}
	posCount += perft(brd, maxDepth)

	t := float64(time.Since(t0)) / 1.0e9
	fmt.Printf("test perft: traversed %d nodes in %.3f s (%.0f nodes/s)\n", posCount, t, float64(posCount)/t)

}

func perft(b *Board, maxDepth int) uint64 {
	perft := make([]uint64, maxDepth+1)
	perft[0] = 1
	perftRecursion(b, 0, maxDepth, perft)
	var sum uint64 = 0
	for i, p := range perft {
		fmt.Printf("test perft: PERFT(%d) = %d\n", i, p)
		sum += p
	}
	return sum
}

func perftRecursion(b *Board, depth int, maxDepth int, perft []uint64) {
	if depth >= maxDepth {
		return
	}
	boards := GetValidMoves(b)
	for i := 0; i < len(boards); i++ {
		perftRecursion(&boards[i], depth+1, maxDepth, perft)
	}
	perft[depth+1] += uint64(len(boards))
}
