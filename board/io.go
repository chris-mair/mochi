/*

	MOCHI - MinOr CHess engIne

	(C) 2022-2024 Chris Mair <chris@1006.org>
	see LICENSE for license information

*/

package board

import (
	"errors"
	"fmt"
	"math"
	"mochi/mochilog"
	"strconv"
	"strings"
)

// Build a Board struct from a FEN string.
// https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation

func FromFEN(fen string) (*Board, error) {

	brd := Board{}
	rank := 7
	file := 0

	parseError := errors.New("FEN parse error")
	invalidPosition := errors.New("FEN invalid position")

	fenPart := strings.Split(fen, " ")
	if len(fenPart) != 6 {
		return nil, parseError
	}

	// part 1: pieces
	cntWhiteKing := 0
	cntWhitePawns := 0
	cntWhitePieces := 0
	cntBlackKing := 0
	cntBlackPawns := 0
	cntBlackPieces := 0
	for _, symbol := range fenPart[0] {
		if symbol == '/' {
			if file != 8 {
				return nil, parseError
			}
			file = 0
			rank--
			continue
		}
		if !isInRange(rank, file) {
			return nil, parseError
		}
		if strings.ContainsRune("PRNBQKprnbqk", symbol) {
			brd.grid[rank][file] = byte(symbol)
			file++
			switch symbol {
			case 'P':
				cntWhitePawns++
				break
			case 'p':
				cntBlackPawns++
				break
			case 'K':
				cntWhiteKing++
				break
			case 'k':
				cntBlackKing++
				break
			}
			if strings.ContainsRune("PRNBQK", symbol) {
				cntWhitePieces++
			}
			if strings.ContainsRune("prnbqk", symbol) {
				cntBlackPieces++
			}
			continue
		}
		if strings.ContainsRune("12345678", symbol) {
			num, _ := strconv.Atoi(string(symbol))
			if file+num > 8 {
				return nil, parseError
			}
			file += num
			continue
		}
		// symbol cannot be something other than valid piece, digit or slash
		return nil, parseError
	}
	if rank != 0 || file != 8 {
		return nil, parseError
	}
	// need exactly one king per side
	if cntWhiteKing != 1 || cntBlackKing != 1 {
		return nil, invalidPosition
	}
	// not more than 8 pawns per side
	if cntWhitePawns > 8 || cntBlackPawns > 8 {
		return nil, invalidPosition
	}
	// not more than 16 pieces per side
	if cntWhitePieces > 16 || cntBlackPieces > 16 {
		return nil, invalidPosition
	}
	// kings cannot touch
	r1, f1 := FindKing(&brd, WHITE)
	r2, f2 := FindKing(&brd, BLACK)
	if math.Abs(float64(r1-r2)) <= 1 && math.Abs(float64(f1-f2)) <= 1 {
		return nil, invalidPosition
	}

	// part 2: side to move
	switch fenPart[1] {
	case "w":
		brd.toMove = WHITE
		break
	case "b":
		brd.toMove = BLACK
		break
	default:
		return nil, parseError
	}
	// consistency regarding checks
	if brd.toMove == WHITE && IsAttacked(&brd, BLACK, r2, f2) {
		return nil, invalidPosition
	}
	if brd.toMove == BLACK && IsAttacked(&brd, WHITE, r1, f1) {
		return nil, invalidPosition
	}
	if IsAttacked(&brd, WHITE, r1, f1) && IsAttacked(&brd, BLACK, r2, f2) {
		return nil, invalidPosition
	}

	// part 3: castling rights
	if fenPart[2] != "-" {
		for _, ch := range fenPart[2] {
			if ch == 'K' {
				if brd.castlingRights&WKS != 0 {
					return nil, parseError
				}
				brd.castlingRights |= WKS
			} else if ch == 'Q' {
				if brd.castlingRights&WQS != 0 {
					return nil, parseError
				}
				brd.castlingRights |= WQS
			} else if ch == 'k' {
				if brd.castlingRights&BKS != 0 {
					return nil, parseError
				}
				brd.castlingRights |= BKS
			} else if ch == 'q' {
				if brd.castlingRights&BQS != 0 {
					return nil, parseError
				}
				brd.castlingRights |= BQS
			} else {
				return nil, parseError
			}
		}
	}

	// part 4: en passant target
	if fenPart[3] == "-" {
		brd.enPassantTargetRank = -1
		brd.enPassantTargetFile = -1
	} else {
		square := fenPart[3]
		if len(square) != 2 {
			return nil, parseError
		}
		file := int(square[0]) - 'a'
		rank := int(square[1]) - '1'
		if !isInRange(rank, file) {
			return nil, parseError
		}
		brd.enPassantTargetRank = int8(rank)
		brd.enPassantTargetFile = int8(file)
	}

	// part 5 and 6: half-moves and move number
	var num int
	var err error
	num, err = strconv.Atoi(fenPart[4])
	if err == nil && num >= 0 && num <= math.MaxUint8 {
		brd.halfMoveClock = uint8(num)
	} else {
		return nil, parseError
	}
	num, err = strconv.Atoi(fenPart[5])
	if err == nil && num >= 0 && num <= math.MaxUint16 {
		// the move number should be >= 1, but I've seen FENs where 0 is given,
		// presumably indicating a "don't care"
		if num == 0 {
			num = 1
		}
		brd.moveNumber = uint16(num)
	} else {
		return nil, parseError
	}

	// reset lastMove and lastMoveKind
	brd.lastMove = NaMInt
	brd.lastMoveKind = MOVETYPE_UNKNOWN

	return &brd, nil

}

func FromStartingPosition() *Board {
	brd, err := FromFEN("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
	if err != nil {
		// should never be reached
		mochilog.Panic("cannot parse starting position")
	}
	return brd
}

func ToString(brd *Board) string {
	var r, f int
	var piece byte

	if brd == nil {
		return "nil board\n"
	}

	ret := ""

	for r = 7; r >= 0; r-- {
		ret += fmt.Sprintf("%d  | ", r+1)
		for f = 0; f <= 7; f++ {
			piece = brd.grid[r][f]
			if piece == 0 {
				piece = '.'
			}
			ret += fmt.Sprintf("%c  ", piece)
		}
		ret += "\n"
	}
	ret += "   +-----------------------\n"
	ret += "     a  b  c  d  e  f  g  h\n"

	ret += fmt.Sprintf("move number %d, ", brd.moveNumber)
	if brd.toMove == WHITE {
		ret += "white to move\n"
	} else {
		ret += "black to move\n"
	}
	ret += "castle done:"
	if brd.castlingDone == 0 {
		ret += " -"
	} else {
		if (brd.castlingDone & WHITE_CASTLED) == WHITE_CASTLED {
			ret += " white"
		}
		if (brd.castlingDone & BLACK_CASTLED) == BLACK_CASTLED {
			ret += " black"
		}
	}
	ret += ", castle rights: "
	if brd.castlingRights == 0 {
		ret += "-"
	} else {
		if (brd.castlingRights & WQS) == WQS {
			ret += "WQS "
		}
		if (brd.castlingRights & WKS) == WKS {
			ret += "WKS "
		}
		if (brd.castlingRights & BQS) == BQS {
			ret += "BQS "
		}
		if (brd.castlingRights & BKS) == BKS {
			ret += "BKS "
		}
	}
	ret += "\n"
	ret += "en passant target square: "
	if brd.enPassantTargetRank == -1 && brd.enPassantTargetFile == -1 {
		ret += "-\n"
	} else {
		ret += fmt.Sprintf("%c%c\n", byte(brd.enPassantTargetFile)+'a', byte(brd.enPassantTargetRank)+'1')
	}
	ret += fmt.Sprintf("half-moves since capture or pawn move: %d\n", brd.halfMoveClock)
	moveStr := "none"
	if brd.lastMove != NaMInt {
		moveStr = MoveToString(brd.lastMove)
	}
	switch brd.lastMoveKind {
	case MOVETYPE_UNKNOWN:
		moveStr += ""
	case MOVETYPE_QUIET:
		moveStr += " (quiet)"
	case MOVETYPE_CAPTURE:
		moveStr += " (capture)"
	case MOVETYPE_CHECK:
		moveStr += " (check)"
	case MOVETYPE_CHECK_WITH_CAPTURE:
		moveStr += " (check with capture)"
	}
	ret += fmt.Sprintf("last move: %s\n", moveStr)
	ret += fmt.Sprintf("eval: %.1f\n", GetEvaluation(brd))
	ret += fmt.Sprintf("hash 0x%x\n", ComputeHash(brd))
	return ret
}

func MoveToString(move uint16) string {
	if move == NaMInt {
		return NaMString
	}
	rankLUT := [8]byte{'1', '2', '3', '4', '5', '6', '7', '8'}
	fileLUT := [8]byte{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'}
	promoteLUT := []string{"", "q", "r", "b", "n"}
	frRank := (move & 0b0000111000000000) >> 9
	frFile := (move & 0b0000000111000000) >> 6
	toRank := (move & 0b0000000000111000) >> 3
	toFile := move & 0b0000000000000111
	ret := string([]byte{fileLUT[frFile], rankLUT[frRank], fileLUT[toFile], rankLUT[toRank]})
	promoteValue := (move & 0b0111000000000000) >> 12
	if promoteValue == 0 {
		return ret
	} else {
		return ret + promoteLUT[promoteValue]
	}
}

func StringToMove(move string) uint16 {
	if move == NaMString {
		return NaMInt
	}
	// Input might come from the UCI GUI, so better check validity.
	// In case it's invalid, just return "?". The caller will then act
	// accordingly and report the error.
	bytes := []byte(move)
	if len(bytes) != 4 && len(bytes) != 5 {
		mochilog.DebugLog(fmt.Sprintf("cannot parse move (length): '%s'", move))
		return NaMInt
	}
	frFile := int(bytes[0] - 'a')
	frRank := int(bytes[1] - '1')
	toFile := int(bytes[2] - 'a')
	toRank := int(bytes[3] - '1')
	if !isInRange(frRank, frFile) || !isInRange(toRank, toFile) {
		mochilog.DebugLog(fmt.Sprintf("cannot parse move (range): '%s'", move))
		return NaMInt
	}
	if len(bytes) == 4 {
		return encodeMove(frRank, frFile, toRank, toFile)
	}
	promote := bytes[4]
	if promote != 'q' && promote != 'r' && promote != 'b' && promote != 'n' {
		mochilog.DebugLog(fmt.Sprintf("cannot parse move (promotion): '%s'", move))
		return NaMInt
	}
	return encodeMoveWithPromotion(frRank, frFile, toRank, toFile, promote)
}
