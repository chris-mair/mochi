/*

	MOCHI - MinOr CHess engIne

	(C) 2022-2024 Chris Mair <chris@1006.org>
	see LICENSE for license information

*/

package board

import (
	"math"
)

// LUT: ASCII piece value -> piece kind
// the highest value ('r') is 114

var kindWhiteLUT [115]int
var kindBlackLUT [115]int

const kindEmpty int = 0
const kindSelfPiece int = 1
const kindSelfKing int = 2
const kindOpponentPiece int = 3
const kindOpponentKing int = 4

func InitLUTs() {
	kindWhiteLUT[0] = kindEmpty
	kindWhiteLUT['P'] = kindSelfPiece
	kindWhiteLUT['R'] = kindSelfPiece
	kindWhiteLUT['N'] = kindSelfPiece
	kindWhiteLUT['B'] = kindSelfPiece
	kindWhiteLUT['Q'] = kindSelfPiece
	kindWhiteLUT['K'] = kindSelfKing
	kindWhiteLUT['p'] = kindOpponentPiece
	kindWhiteLUT['r'] = kindOpponentPiece
	kindWhiteLUT['n'] = kindOpponentPiece
	kindWhiteLUT['b'] = kindOpponentPiece
	kindWhiteLUT['q'] = kindOpponentPiece
	kindWhiteLUT['k'] = kindOpponentKing

	kindBlackLUT[0] = kindEmpty
	kindBlackLUT['P'] = kindOpponentPiece
	kindBlackLUT['R'] = kindOpponentPiece
	kindBlackLUT['N'] = kindOpponentPiece
	kindBlackLUT['B'] = kindOpponentPiece
	kindBlackLUT['Q'] = kindOpponentPiece
	kindBlackLUT['K'] = kindOpponentKing
	kindBlackLUT['p'] = kindSelfPiece
	kindBlackLUT['r'] = kindSelfPiece
	kindBlackLUT['n'] = kindSelfPiece
	kindBlackLUT['b'] = kindSelfPiece
	kindBlackLUT['q'] = kindSelfPiece
	kindBlackLUT['k'] = kindSelfKing

}

func GetValidMoves(brd *Board) []Board {

	side := brd.toMove

	var direction int

	var kind []int

	if side == WHITE {
		direction = 1
		kind = kindWhiteLUT[:]
	} else {
		direction = -1
		kind = kindBlackLUT[:]
	}

	myKingRank, myKingFile := FindKing(brd, side)

	candidateBoards := make([]Board, 0, 60)
	parentPieceCount := 0

	for rank := 0; rank <= 7; rank++ {
		for file := 0; file <= 7; file++ {

			piece := brd.grid[rank][file]

			if piece == 0 {
				continue
			}

			parentPieceCount++

			if kind[piece] == kindOpponentPiece || kind[piece] == kindOpponentKing {
				continue
			}

			if piece == 'P' || piece == 'p' {

				// --- pawns -------------------------------------------------------------------------------------------

				if (piece == 'P' && rank == 1) || (piece == 'p' && rank == 6) {
					// pawns that move two squares
					jumpRank := rank + direction
					targetRank := rank + 2*direction
					if kind[brd.grid[jumpRank][file]] == kindEmpty && kind[brd.grid[targetRank][file]] == kindEmpty {
						newBoard := *brd
						newBoard.grid[rank][file] = 0
						// newBoard.quickEval does not change
						newBoard.grid[targetRank][file] = piece
						if !IsAttacked(&newBoard, side, myKingRank, myKingFile) {
							newBoard.enPassantTargetRank = int8(jumpRank)
							newBoard.enPassantTargetFile = int8(file)
							newBoard.lastMove = encodeMove(rank, file, targetRank, file)
							newBoard.lastMoveKind = MOVETYPE_QUIET
							newBoard.halfMoveClock = 0
							candidateBoards = append(candidateBoards, newBoard)
						}
					}
				}

				if (piece == 'P' && rank < 6) || (piece == 'p' && rank > 1) {
					// pawns that move one square or take without promoting
					targetRank := rank + direction
					// ... move one square
					if kind[brd.grid[targetRank][file]] == kindEmpty {
						newBoard := *brd
						newBoard.grid[rank][file] = 0
						newBoard.grid[targetRank][file] = piece
						if !IsAttacked(&newBoard, side, myKingRank, myKingFile) {
							newBoard.enPassantTargetRank = -1
							newBoard.enPassantTargetFile = -1
							newBoard.lastMove = encodeMove(rank, file, targetRank, file)
							newBoard.lastMoveKind = MOVETYPE_QUIET
							newBoard.halfMoveClock = 0
							candidateBoards = append(candidateBoards, newBoard)
						}
					}
					// ... take without promoting
					for df := -1; df <= 1; df += 2 {
						targetFile := file + df
						if isInRange(targetRank, targetFile) && kind[brd.grid[targetRank][targetFile]] == kindOpponentPiece {
							newBoard := *brd
							newBoard.grid[rank][file] = 0
							newBoard.grid[targetRank][targetFile] = piece
							if !IsAttacked(&newBoard, side, myKingRank, myKingFile) {
								newBoard.enPassantTargetRank = -1
								newBoard.enPassantTargetFile = -1
								newBoard.lastMove = encodeMove(rank, file, targetRank, targetFile)
								newBoard.lastMoveKind = MOVETYPE_CAPTURE
								newBoard.halfMoveClock = 0
								candidateBoards = append(candidateBoards, newBoard)
							}
						}
					}
				}

				if (piece == 'P' && rank == 6) || (piece == 'p' && rank == 1) {
					// pawns that promote
					targetRank := rank + direction
					var targetPiece [4]byte
					if piece == 'P' {
						targetPiece = [4]byte{'R', 'N', 'B', 'Q'}
					} else {
						targetPiece = [4]byte{'r', 'n', 'b', 'q'}
					}
					// ... no takes
					if kind[brd.grid[targetRank][file]] == kindEmpty {
						for i := 0; i < 4; i++ {
							newBoard := *brd
							newBoard.grid[rank][file] = 0
							newBoard.grid[targetRank][file] = targetPiece[i]
							if !IsAttacked(&newBoard, side, myKingRank, myKingFile) {
								newBoard.enPassantTargetRank = -1
								newBoard.enPassantTargetFile = -1
								newBoard.lastMove = encodeMoveWithPromotion(rank, file, targetRank, file, targetPiece[i])
								newBoard.lastMoveKind = MOVETYPE_QUIET
								newBoard.halfMoveClock = 0
								candidateBoards = append(candidateBoards, newBoard)
							}
						}
					}
					// ... takes
					for df := -1; df <= 1; df += 2 {
						targetFile := file + df
						if isInRange(targetRank, targetFile) && kind[brd.grid[targetRank][targetFile]] == kindOpponentPiece {
							for i := 0; i < 4; i++ {
								newBoard := *brd
								newBoard.grid[rank][file] = 0
								newBoard.grid[targetRank][targetFile] = targetPiece[i]
								if !IsAttacked(&newBoard, side, myKingRank, myKingFile) {
									newBoard.enPassantTargetRank = -1
									newBoard.enPassantTargetFile = -1
									newBoard.lastMove = encodeMoveWithPromotion(rank, file, targetRank, targetFile, targetPiece[i])
									newBoard.lastMoveKind = MOVETYPE_CAPTURE
									newBoard.halfMoveClock = 0
									candidateBoards = append(candidateBoards, newBoard)
								}
							}
						}
					}
				}

				if (piece == 'P' && rank == 4) || (piece == 'p' && rank == 3) {
					// pawns that take en passant
					targetRank := rank + direction
					if int(brd.enPassantTargetRank) == targetRank && math.Abs(float64(brd.enPassantTargetFile)-float64(file)) == 1 {
						newBoard := *brd
						newBoard.grid[rank][file] = 0
						newBoard.grid[rank][brd.enPassantTargetFile] = 0
						newBoard.grid[targetRank][brd.enPassantTargetFile] = piece
						if !IsAttacked(&newBoard, side, myKingRank, myKingFile) {
							newBoard.enPassantTargetRank = -1
							newBoard.enPassantTargetFile = -1
							newBoard.lastMove = encodeMove(rank, file, targetRank, int(brd.enPassantTargetFile))
							newBoard.lastMoveKind = MOVETYPE_CAPTURE
							newBoard.halfMoveClock = 0
							candidateBoards = append(candidateBoards, newBoard)
						}
					}
				}

			} else if piece == 'N' || piece == 'n' {

				// --- knights -----------------------------------------------------------------------------------------

				nrList := [8]int{-2, -1, 1, 2, 2, 1, -1, -2}
				nfList := [8]int{-1, -2, -2, -1, 1, 2, 2, 1}
				for i := 0; i < 8; i++ {
					targetRank := rank + nrList[i]
					targetFile := file + nfList[i]
					if !isInRange(targetRank, targetFile) {
						continue
					}
					targetPiece := brd.grid[targetRank][targetFile]
					if kind[targetPiece] == kindEmpty || kind[targetPiece] == kindOpponentPiece {
						newBoard := *brd
						newBoard.grid[rank][file] = 0
						newBoard.grid[targetRank][targetFile] = piece
						if !IsAttacked(&newBoard, side, myKingRank, myKingFile) {
							newBoard.enPassantTargetRank = -1
							newBoard.enPassantTargetFile = -1
							newBoard.lastMove = encodeMove(rank, file, targetRank, targetFile)
							if kind[targetPiece] == kindEmpty {
								newBoard.lastMoveKind = MOVETYPE_QUIET
								newBoard.halfMoveClock++
							} else {
								newBoard.lastMoveKind = MOVETYPE_CAPTURE
								newBoard.halfMoveClock = 0
							}
							candidateBoards = append(candidateBoards, newBoard)
						}
					}
				}

			} else if piece == 'R' || piece == 'r' || piece == 'B' || piece == 'b' || piece == 'Q' || piece == 'q' || piece == 'K' || piece == 'k' {

				// --- rooks, bishops, queens or kings -----------------------------------------------------------------

				var drList []int
				var dfList []int

				if piece == 'R' || piece == 'r' {
					drList = []int{-1, 1, 0, 0}
					dfList = []int{0, 0, -1, 1}
				} else if piece == 'B' || piece == 'b' {
					drList = []int{-1, -1, 1, 1}
					dfList = []int{-1, 1, -1, 1}
				} else {
					drList = []int{-1, 1, 0, 0, -1, -1, 1, 1}
					dfList = []int{0, 0, -1, 1, -1, 1, -1, 1}
				}

				for i := 0; i < len(drList); i++ {
					targetRank := rank
					targetFile := file
					for {
						targetRank += drList[i]
						targetFile += dfList[i]
						if !isInRange(targetRank, targetFile) {
							break
						}
						targetPiece := brd.grid[targetRank][targetFile]
						if kind[targetPiece] == kindEmpty || kind[targetPiece] == kindOpponentPiece {
							newBoard := *brd
							newBoard.grid[rank][file] = 0
							newBoard.grid[targetRank][targetFile] = piece
							newBoard.enPassantTargetRank = -1
							newBoard.enPassantTargetFile = -1
							newBoard.lastMove = encodeMove(rank, file, targetRank, targetFile)
							tempMyKingRank := myKingRank
							tempMyKingFile := myKingFile
							if piece == 'K' || piece == 'k' {
								tempMyKingRank = targetRank
								tempMyKingFile = targetFile
							}
							if !IsAttacked(&newBoard, side, tempMyKingRank, tempMyKingFile) {
								// remove castling rights if kings are moved
								if piece == 'K' {
									newBoard.castlingRights = newBoard.castlingRights &^ WQS
									newBoard.castlingRights = newBoard.castlingRights &^ WKS
								} else if piece == 'k' {
									newBoard.castlingRights = newBoard.castlingRights &^ BQS
									newBoard.castlingRights = newBoard.castlingRights &^ BKS
								}
								if kind[targetPiece] == kindEmpty {
									newBoard.lastMoveKind = MOVETYPE_QUIET
									newBoard.halfMoveClock++
								} else {
									newBoard.lastMoveKind = MOVETYPE_CAPTURE
									newBoard.halfMoveClock = 0
								}
								candidateBoards = append(candidateBoards, newBoard)
							}

						}
						if kind[brd.grid[targetRank][targetFile]] != kindEmpty {
							break
						}
						if piece == 'K' || piece == 'k' {
							break
						}
					}
				}
			}

			if piece == 'K' {

				// --- white castle ------------------------------------------------------------------------------------

				if (brd.castlingRights & WKS) == WKS {
					if brd.grid[0][5] == 0 && brd.grid[0][6] == 0 &&
						!IsAttacked(brd, side, 0, 4) && !IsAttacked(brd, side, 0, 5) && !IsAttacked(brd, side, 0, 6) {
						newBoard := *brd
						newBoard.grid[0][4] = 0
						newBoard.grid[0][6] = 'K'
						newBoard.grid[0][7] = 0
						newBoard.grid[0][5] = 'R'
						newBoard.enPassantTargetRank = -1
						newBoard.enPassantTargetFile = -1
						newBoard.castlingRights = newBoard.castlingRights &^ WKS
						newBoard.castlingRights = newBoard.castlingRights &^ WQS
						newBoard.lastMove = encodeMove(0, 4, 0, 6)
						newBoard.castlingDone = newBoard.castlingDone | WHITE_CASTLED
						newBoard.lastMoveKind = MOVETYPE_QUIET
						newBoard.halfMoveClock++
						candidateBoards = append(candidateBoards, newBoard)
					}
				}
				if (brd.castlingRights & WQS) == WQS {
					if brd.grid[0][1] == 0 && brd.grid[0][2] == 0 && brd.grid[0][3] == 0 &&
						!IsAttacked(brd, side, 0, 2) && !IsAttacked(brd, side, 0, 3) && !IsAttacked(brd, side, 0, 4) {
						newBoard := *brd
						newBoard.grid[0][4] = 0
						newBoard.grid[0][2] = 'K'
						newBoard.grid[0][0] = 0
						newBoard.grid[0][3] = 'R'
						newBoard.enPassantTargetRank = -1
						newBoard.enPassantTargetFile = -1
						newBoard.castlingRights = newBoard.castlingRights &^ WKS
						newBoard.castlingRights = newBoard.castlingRights &^ WQS
						newBoard.lastMove = encodeMove(0, 4, 0, 2)
						newBoard.castlingDone = newBoard.castlingDone | WHITE_CASTLED
						newBoard.lastMoveKind = MOVETYPE_QUIET
						newBoard.halfMoveClock++
						candidateBoards = append(candidateBoards, newBoard)
					}
				}

			} else if piece == 'k' {

				// --- black castle ------------------------------------------------------------------------------------

				if (brd.castlingRights & BKS) == BKS {
					if brd.grid[7][5] == 0 && brd.grid[7][6] == 0 &&
						!IsAttacked(brd, side, 7, 4) && !IsAttacked(brd, side, 7, 5) && !IsAttacked(brd, side, 7, 6) {
						newBoard := *brd
						newBoard.grid[7][4] = 0
						newBoard.grid[7][6] = 'k'
						newBoard.grid[7][7] = 0
						newBoard.grid[7][5] = 'r'
						newBoard.enPassantTargetRank = -1
						newBoard.enPassantTargetFile = -1
						newBoard.castlingRights = newBoard.castlingRights &^ BKS
						newBoard.castlingRights = newBoard.castlingRights &^ BQS
						newBoard.lastMove = encodeMove(7, 4, 7, 6)
						newBoard.castlingDone = newBoard.castlingDone | BLACK_CASTLED
						newBoard.lastMoveKind = MOVETYPE_QUIET
						newBoard.halfMoveClock++
						candidateBoards = append(candidateBoards, newBoard)
					}
				}
				if (brd.castlingRights & BQS) == BQS {
					if brd.grid[7][1] == 0 && brd.grid[7][2] == 0 && brd.grid[7][3] == 0 &&
						!IsAttacked(brd, side, 7, 2) && !IsAttacked(brd, side, 7, 3) && !IsAttacked(brd, side, 7, 4) {
						newBoard := *brd
						newBoard.grid[7][4] = 0
						newBoard.grid[7][2] = 'k'
						newBoard.grid[7][0] = 0
						newBoard.grid[7][3] = 'r'
						newBoard.enPassantTargetRank = -1
						newBoard.enPassantTargetFile = -1
						newBoard.castlingRights = newBoard.castlingRights &^ BKS
						newBoard.castlingRights = newBoard.castlingRights &^ BQS
						newBoard.lastMove = encodeMove(7, 4, 7, 2)
						newBoard.castlingDone = newBoard.castlingDone | BLACK_CASTLED
						newBoard.lastMoveKind = MOVETYPE_QUIET
						newBoard.halfMoveClock++
						candidateBoards = append(candidateBoards, newBoard)
					}
				}
			}

		}
	}

	otherSide := toggleSide(side)

	for i := 0; i < len(candidateBoards); i++ {

		// now it's the opposite side to move
		candidateBoards[i].toMove = otherSide

		// increment move counter
		if candidateBoards[i].toMove == WHITE {
			candidateBoards[i].moveNumber++
		}

		// Adjust move type for checks.
		kingRank, kingFile := FindKing(&(candidateBoards[i]), otherSide)
		if IsAttacked(&(candidateBoards[i]), otherSide, kingRank, kingFile) {
			if candidateBoards[i].lastMoveKind == MOVETYPE_CAPTURE {
				candidateBoards[i].lastMoveKind = MOVETYPE_CHECK_WITH_CAPTURE
			} else {
				candidateBoards[i].lastMoveKind = MOVETYPE_CHECK
			}
		}

		// We need to remove castling rights if the rooks were moved or taken. There are plenty
		// of places in the code where a rook can be taken, so instead of adding ifs all over
		// the place, we unconditionally remove castling rights here if the rooks are not in their
		// home squares.
		if candidateBoards[i].grid[0][0] != 'R' {
			candidateBoards[i].castlingRights = candidateBoards[i].castlingRights &^ WQS
		}
		if candidateBoards[i].grid[0][7] != 'R' {
			candidateBoards[i].castlingRights = candidateBoards[i].castlingRights &^ WKS
		}
		if candidateBoards[i].grid[7][0] != 'r' {
			candidateBoards[i].castlingRights = candidateBoards[i].castlingRights &^ BQS
		}
		if candidateBoards[i].grid[7][7] != 'r' {
			candidateBoards[i].castlingRights = candidateBoards[i].castlingRights &^ BKS
		}
	}

	return candidateBoards
}
