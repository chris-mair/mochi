/*

	MOCHI - MinOr CHess engIne

	(C) 2022-2024 Chris Mair <chris@1006.org>
	see LICENSE for license information

*/

package board

import (
	"errors"
	"mochi/mochilog"
)

func GetLastMove(brd *Board) uint16 {
	return brd.lastMove
}

func GetLastMoveKind(brd *Board) LastMoveType {
	return brd.lastMoveKind
}

func GetSideToMove(brd *Board) SideType {
	return brd.toMove
}

func ApplyMove(brd *Board, moveStr string) error {
	move := StringToMove(moveStr)
	boards := GetValidMoves(brd)
	ix := -1
	for i := 0; i < len(boards); i++ {
		if GetLastMove(&(boards[i])) == move {
			ix = i
			break
		}
	}
	if ix == -1 {
		return errors.New("invalid move")
	}
	*brd = boards[ix]
	return nil
}

var drDiag = [4]int{-1, -1, 1, 1}
var dfDiag = [4]int{-1, 1, -1, 1}
var drStrt = [4]int{-1, 1, 0, 0}
var dfStrt = [4]int{0, 0, -1, 1}
var drLshp = [8]int{-2, -1, 1, 2, 2, 1, -1, -2}
var dfLshp = [8]int{-1, -2, -2, -1, 1, 2, 2, 1}
var drKing = [8]int{-1, -1, -1, 0, 0, 1, 1, 1}
var dfKing = [8]int{-1, 0, 1, -1, 1, -1, 0, 1}

func IsWhiteDefended(brd *Board, rank int, file int) bool {
	var piece byte

	// check pawns
	if isInRange(rank-1, file-1) && brd.grid[rank-1][file-1] == 'P' {
		return true
	}
	if isInRange(rank-1, file+1) && brd.grid[rank-1][file+1] == 'P' {
		return true
	}

	// check 4 diagonals
	for i := 0; i < 4; i++ {
		dr := drDiag[i]
		df := dfDiag[i]
		r, f := rank, file
		for {
			r += dr
			f += df
			if !isInRange(r, f) {
				break
			}
			piece = brd.grid[r][f]
			if piece == 0 {
				continue
			}
			if piece == 'Q' || piece == 'B' {
				return true
			}
			break
		}
	}

	// check 4 straight lines
	for i := 0; i < 4; i++ {
		dr := drStrt[i]
		df := dfStrt[i]
		r, f := rank, file
		for {
			r += dr
			f += df
			if !isInRange(r, f) {
				break
			}
			piece = brd.grid[r][f]
			if piece == 0 {
				continue
			}
			if piece == 'Q' || piece == 'R' {
				return true
			}
			break
		}
	}

	// check 8 knights
	for i := 0; i < 8; i++ {
		r := rank + drLshp[i]
		f := file + dfLshp[i]
		if !isInRange(r, f) {
			continue
		}
		piece = brd.grid[r][f]
		if piece == 'N' {
			return true
		}
	}

	// check 8 kings
	for i := 0; i < 8; i++ {
		r := rank + drKing[i]
		f := file + dfKing[i]
		if !isInRange(r, f) {
			continue
		}
		piece = brd.grid[r][f]
		if piece == 'K' {
			return true
		}
	}

	return false
}

func IsBlackDefended(brd *Board, rank int, file int) bool {
	var piece byte

	// check pawns
	if isInRange(rank+1, file-1) && brd.grid[rank+1][file-1] == 'p' {
		return true
	}
	if isInRange(rank+1, file+1) && brd.grid[rank+1][file+1] == 'p' {
		return true
	}

	// check 4 diagonals
	for i := 0; i < 4; i++ {
		dr := drDiag[i]
		df := dfDiag[i]
		r, f := rank, file
		for {
			r += dr
			f += df
			if !isInRange(r, f) {
				break
			}
			piece = brd.grid[r][f]
			if piece == 0 {
				continue
			}
			if piece == 'q' || piece == 'b' {
				return true
			}
			break
		}
	}

	// check 4 straight lines
	for i := 0; i < 4; i++ {
		dr := drStrt[i]
		df := dfStrt[i]
		r, f := rank, file
		for {
			r += dr
			f += df
			if !isInRange(r, f) {
				break
			}
			piece = brd.grid[r][f]
			if piece == 0 {
				continue
			}
			if piece == 'q' || piece == 'r' {
				return true
			}
			break
		}
	}

	// check 8 knights
	for i := 0; i < 8; i++ {
		r := rank + drLshp[i]
		f := file + dfLshp[i]
		if !isInRange(r, f) {
			continue
		}
		piece = brd.grid[r][f]
		if piece == 'n' {
			return true
		}
	}

	// check 8 kings
	for i := 0; i < 8; i++ {
		r := rank + drKing[i]
		f := file + dfKing[i]
		if !isInRange(r, f) {
			continue
		}
		piece = brd.grid[r][f]
		if piece == 'k' {
			return true
		}
	}

	return false
}

func IsAttacked(brd *Board, side SideType, rank int, file int) bool {

	var piece byte
	var foeQueen, foeBishop, foeRook, foeKnight, foeKing byte

	if side == WHITE {
		foeQueen = 'q'
		foeBishop = 'b'
		foeRook = 'r'
		foeKnight = 'n'
		foeKing = 'k'
	} else {
		foeQueen = 'Q'
		foeBishop = 'B'
		foeRook = 'R'
		foeKnight = 'N'
		foeKing = 'K'
	}

	// check 2 pawns
	if side == WHITE {
		if isInRange(rank+1, file-1) && brd.grid[rank+1][file-1] == 'p' {
			return true
		}
		if isInRange(rank+1, file+1) && brd.grid[rank+1][file+1] == 'p' {
			return true
		}
	} else {
		if isInRange(rank-1, file-1) && brd.grid[rank-1][file-1] == 'P' {
			return true
		}
		if isInRange(rank-1, file+1) && brd.grid[rank-1][file+1] == 'P' {
			return true
		}
	}

	// check 4 diagonals
	for i := 0; i < 4; i++ {
		dr := drDiag[i]
		df := dfDiag[i]
		r, f := rank, file
		for {
			r += dr
			f += df
			if !isInRange(r, f) {
				break
			}
			piece = brd.grid[r][f]
			if piece == 0 {
				continue
			}
			if piece == foeQueen || piece == foeBishop {
				return true
			}
			break
		}
	}

	// check 4 straight lines
	for i := 0; i < 4; i++ {
		dr := drStrt[i]
		df := dfStrt[i]
		r, f := rank, file
		for {
			r += dr
			f += df
			if !isInRange(r, f) {
				break
			}
			piece = brd.grid[r][f]
			if piece == 0 {
				continue
			}
			if piece == foeQueen || piece == foeRook {
				return true
			}
			break
		}
	}

	// check 8 knights
	for i := 0; i < 8; i++ {
		r := rank + drLshp[i]
		f := file + dfLshp[i]
		if !isInRange(r, f) {
			continue
		}
		piece = brd.grid[r][f]
		if piece == foeKnight {
			return true
		}
	}

	// check 8 kings
	for i := 0; i < 8; i++ {
		r := rank + drKing[i]
		f := file + dfKing[i]
		if !isInRange(r, f) {
			continue
		}
		piece = brd.grid[r][f]
		if piece == foeKing {
			return true
		}
	}

	return false
}

func FindKing(brd *Board, side SideType) (int, int) {
	var rank, file int
	if side == WHITE {
		for rank = 0; rank < 8; rank++ {
			for file = 0; file < 8; file++ {
				if brd.grid[rank][file] == 'K' {
					return rank, file
				}
			}
		}
	} else {
		for rank = 7; rank >= 0; rank-- {
			for file = 0; file < 8; file++ {
				if brd.grid[rank][file] == 'k' {
					return rank, file
				}
			}
		}
	}
	// should never be reached
	mochilog.Panic("no king")
	return 0, 0
}

func encodeMove(fromRank int, fromFile int, toRank int, toFile int) uint16 {
	return uint16(fromRank<<9 + fromFile<<6 + toRank<<3 + toFile)
}

func encodeMoveWithPromotion(fromRank int, fromFile int, toRank int, toFile int, promote byte) uint16 {
	var promoteValue uint16 = 0
	if promote == 'q' || promote == 'Q' {
		promoteValue = 1
	} else if promote == 'r' || promote == 'R' {
		promoteValue = 2
	} else if promote == 'b' || promote == 'B' {
		promoteValue = 3
	} else if promote == 'n' || promote == 'N' {
		promoteValue = 4
	}
	return uint16(fromRank<<9+fromFile<<6+toRank<<3+toFile) + (promoteValue << 12)
}

func isInRange(rank int, file int) bool {
	// equivalent to rank >= 0 && rank <= 7 && file >= 0 && file <= 7
	return (rank|file)&0xfffffff8 == 0
}

func toggleSide(side SideType) SideType {
	if side == WHITE {
		return BLACK
	} else {
		return WHITE
	}
}
