/*

	MOCHI - MinOr CHess engIne

	(C) 2022-2024 Chris Mair <chris@1006.org>
	see LICENSE for license information

*/

package board

import "math"

func GetEvaluation(brd *Board) float32 {

	const VALUE_PAWN float32 = 100.0
	const VALUE_KNIGHT float32 = 300.0
	const VALUE_KNIGHT_ON_RIM float32 = 285.0
	const VALUE_BISHOP float32 = 300.0
	const VALUE_ROOK float32 = 500.0
	const VALUE_QUEEN float32 = 900.0

	const BONUS_CONNECTED_ROOKS float32 = 15.0
	const BONUS_ROOK_OPEN_LINE_PER_FIELD float32 = 1.5
	const BONUS_BISHOP_OPEN_LINE_PER_FIELD float32 = 2.5
	const BONUS_PAWN_CHAIN_PER_LINK float32 = 5.0
	const BONUS_PAWN_ADVANCE_PER_SQUARE float32 = 10.0
	const BONUS_CASTLING float32 = 75.0
	const BONUS_BISHOP_PAIR float32 = 25.0

	const PENALTY_EARLY_QUEEN_MOVE float32 = 25.0
	const PENALTY_SLOW_DEVELOPMENT_PER_PIECE float32 = 15.0
	const PENALTY_DOUBLED_PAWN float32 = 20.0
	const PENALTY_ISOLATED_PAWN float32 = 20.0
	const PENALTY_CASTLING_RIGHTS_LOST float32 = 25.0
	const PENALTY_KING_SAFETY float32 = 30.0
	const PENALTY_UNPROTECTED_FACTOR float32 = 0.025
	const PENALTY_SHIELD_PAWN_MISSING = 20.0

	// NOTE: stalemate or checkmate are not detected here,
	// they are detected in the tree search (see compute.go).

	// draw by 50-move rule
	if brd.halfMoveClock >= 100 {
		return 0.0
	}

	var val float32 = 0.0

	whiteCount := 0
	blackCount := 0
	whiteBishopCount := 0
	blackBishopCount := 0

	for rank := 0; rank <= 7; rank++ {
		for file := 0; file <= 7; file++ {

			switch brd.grid[rank][file] {
			case '0':
				break
			case 'P':
				whiteCount++
				val += VALUE_PAWN
				// in the early phase of the game, only central pawns increase their
				// value as they advance, later all pawns do
				if brd.moveNumber >= 20 || (file == 3 || file == 4) {
					val += BONUS_PAWN_ADVANCE_PER_SQUARE * (float32(rank - 1))
				}
				// incentivate pawn chains
				if file > 0 && brd.grid[rank-1][file-1] == 'P' {
					val += BONUS_PAWN_CHAIN_PER_LINK
				}
				if file < 7 && brd.grid[rank-1][file+1] == 'P' {
					val += BONUS_PAWN_CHAIN_PER_LINK
				}
				// disincentivate doubled pawns
				for r := rank - 1; r >= 1; r-- {
					if brd.grid[r][file] == 'P' {
						val -= PENALTY_DOUBLED_PAWN
						break
					}
				}
				// disincentivate isolated pawns
				peer_found := false
				for r := 1; r <= 7; r++ {
					if (file > 0 && brd.grid[r][file-1] == 'P') || (file < 7 && brd.grid[r][file+1] == 'P') {
						peer_found = true
						break
					}
				}
				if !peer_found {
					val -= PENALTY_ISOLATED_PAWN
				}
			case 'p':
				blackCount++
				val -= VALUE_PAWN
				// in the early phase of the game, only central pawns increase their
				// value as they advance, later all pawns do
				if brd.moveNumber >= 20 || (file == 3 || file == 4) {
					val -= BONUS_PAWN_ADVANCE_PER_SQUARE * (float32(6 - rank))
				}
				// incentivate pawn chains
				if file > 0 && brd.grid[rank+1][file-1] == 'p' {
					val -= BONUS_PAWN_CHAIN_PER_LINK
				}
				if file < 7 && brd.grid[rank+1][file+1] == 'p' {
					val -= BONUS_PAWN_CHAIN_PER_LINK
				}
				// disincentivate doubled pawns
				for r := rank + 1; r <= 6; r++ {
					if brd.grid[r][file] == 'p' {
						val += PENALTY_DOUBLED_PAWN
						break
					}
				}
				// disincentivate isolated pawns
				peer_found := false
				for r := 1; r <= 7; r++ {
					if (file > 0 && brd.grid[r][file-1] == 'p') || (file < 7 && brd.grid[r][file+1] == 'p') {
						peer_found = true
						break
					}
				}
				if !peer_found {
					val += PENALTY_ISOLATED_PAWN
				}
			case 'R':
				whiteCount++
				// white rook
				val += VALUE_ROOK
				// extra points for open ranks and files and connected rooks
				for r := rank + 1; r <= 7; r++ {
					if brd.grid[r][file] == 'R' {
						val += BONUS_CONNECTED_ROOKS
						break
					}
					if !(brd.grid[r][file] == 0) {
						break
					}
					val += BONUS_ROOK_OPEN_LINE_PER_FIELD
				}
				for r := rank - 1; r >= 0; r-- {
					if brd.grid[r][file] == 'R' {
						val += BONUS_CONNECTED_ROOKS
						break
					}
					if !(brd.grid[r][file] == 0) {
						break
					}
					val += BONUS_ROOK_OPEN_LINE_PER_FIELD
				}
				for f := file + 1; f <= 7; f++ {
					if brd.grid[rank][f] == 'R' {
						val += BONUS_CONNECTED_ROOKS
						break
					}
					if !(brd.grid[rank][f] == 0) {
						break
					}
					val += BONUS_ROOK_OPEN_LINE_PER_FIELD
				}
				for f := file - 1; f >= 0; f-- {
					if brd.grid[rank][f] == 'R' {
						val += BONUS_CONNECTED_ROOKS
						break
					}
					if !(brd.grid[rank][f] == 0) {
						break
					}
					val += BONUS_ROOK_OPEN_LINE_PER_FIELD
				}
				// penalty if left unprotected
				if !IsWhiteDefended(brd, rank, file) {
					val -= VALUE_ROOK * PENALTY_UNPROTECTED_FACTOR
				}
			case 'r':
				blackCount++
				// black rook
				val -= VALUE_ROOK
				// extra points for open ranks and files and connected rooks
				for r := rank + 1; r <= 7; r++ {
					if brd.grid[r][file] == 'r' {
						val -= BONUS_CONNECTED_ROOKS
						break
					}
					if !(brd.grid[r][file] == 0) {
						break
					}
					val -= BONUS_ROOK_OPEN_LINE_PER_FIELD
				}
				for r := rank - 1; r >= 0; r-- {
					if brd.grid[r][file] == 'r' {
						val -= BONUS_CONNECTED_ROOKS
						break
					}
					if !(brd.grid[r][file] == 0) {
						break
					}
					val -= BONUS_ROOK_OPEN_LINE_PER_FIELD
				}
				for f := file + 1; f <= 7; f++ {
					if brd.grid[rank][f] == 'r' {
						val -= BONUS_CONNECTED_ROOKS
						break
					}
					if !(brd.grid[rank][f] == 0) {
						break
					}
					val -= BONUS_ROOK_OPEN_LINE_PER_FIELD
				}
				for f := file - 1; f >= 0; f-- {
					if brd.grid[rank][f] == 'r' {
						val -= BONUS_CONNECTED_ROOKS
						break
					}
					if !(brd.grid[rank][f] == 0) {
						break
					}
					val -= BONUS_ROOK_OPEN_LINE_PER_FIELD
				}
				// penalty if left unprotected
				if !IsBlackDefended(brd, rank, file) {
					val += VALUE_ROOK * PENALTY_UNPROTECTED_FACTOR
				}
			case 'B':
				whiteCount++
				whiteBishopCount++
				// white bishop
				val += VALUE_BISHOP
				// extra points for mobility
				for d := 1; d <= 7; d++ {
					if rank+d > 7 || file+d > 7 {
						break
					}
					if !(brd.grid[rank+d][file+d] == 0) {
						break
					}
					val += BONUS_BISHOP_OPEN_LINE_PER_FIELD
				}
				for d := 1; d <= 7; d++ {
					if rank+d > 7 || file-d < 0 {
						break
					}
					if !(brd.grid[rank+d][file-d] == 0) {
						break
					}
					val += BONUS_BISHOP_OPEN_LINE_PER_FIELD
				}
				for d := 1; d <= 7; d++ {
					if rank-d < 0 || file+d > 7 {
						break
					}
					if !(brd.grid[rank-d][file+d] == 0) {
						break
					}
					val += BONUS_BISHOP_OPEN_LINE_PER_FIELD
				}
				for d := 1; d <= 7; d++ {
					if rank-d < 0 || file-d < 0 {
						break
					}
					if !(brd.grid[rank-d][file-d] == 0) {
						break
					}
					val += BONUS_BISHOP_OPEN_LINE_PER_FIELD
				}
				// penalty if left unprotected
				if !IsWhiteDefended(brd, rank, file) {
					val -= VALUE_BISHOP * PENALTY_UNPROTECTED_FACTOR
				}
			case 'b':
				blackCount++
				blackBishopCount++
				// black bishop
				val -= VALUE_BISHOP
				// extra points for mobility
				for d := 1; d <= 7; d++ {
					if rank+d > 7 || file+d > 7 {
						break
					}
					if !(brd.grid[rank+d][file+d] == 0) {
						break
					}
					val -= BONUS_BISHOP_OPEN_LINE_PER_FIELD
				}
				for d := 1; d <= 7; d++ {
					if rank+d > 7 || file-d < 0 {
						break
					}
					if !(brd.grid[rank+d][file-d] == 0) {
						break
					}
					val -= BONUS_BISHOP_OPEN_LINE_PER_FIELD
				}
				for d := 1; d <= 7; d++ {
					if rank-d < 0 || file+d > 7 {
						break
					}
					if !(brd.grid[rank-d][file+d] == 0) {
						break
					}
					val -= BONUS_BISHOP_OPEN_LINE_PER_FIELD
				}
				for d := 1; d <= 7; d++ {
					if rank-d < 0 || file-d < 0 {
						break
					}
					if !(brd.grid[rank-d][file-d] == 0) {
						break
					}
					val -= BONUS_BISHOP_OPEN_LINE_PER_FIELD
				}
				// penalty if left unprotected
				if !IsBlackDefended(brd, rank, file) {
					val += VALUE_BISHOP * PENALTY_UNPROTECTED_FACTOR
				}
			case 'N':
				whiteCount++
				// white knights
				if file == 0 || file == 7 || rank == 0 || rank == 7 {
					val += VALUE_KNIGHT_ON_RIM
				} else {
					val += VALUE_KNIGHT
				}
				// penalty if left unprotected
				if !IsWhiteDefended(brd, rank, file) {
					val -= VALUE_KNIGHT * PENALTY_UNPROTECTED_FACTOR
				}
			case 'n':
				blackCount++
				// black knights
				if file == 0 || file == 7 || rank == 0 || rank == 7 {
					val -= VALUE_KNIGHT_ON_RIM
				} else {
					val -= VALUE_KNIGHT
				}
				// penalty if left unprotected
				if !IsBlackDefended(brd, rank, file) {
					val += VALUE_KNIGHT * PENALTY_UNPROTECTED_FACTOR
				}
			case 'Q':
				whiteCount++
				// white queen
				val += VALUE_QUEEN
				// penalty for moving too early
				if brd.moveNumber < 10 && (rank != 0 || file != 3) {
					val -= PENALTY_EARLY_QUEEN_MOVE
				}
				// penalty if left unprotected
				if !IsWhiteDefended(brd, rank, file) {
					val -= VALUE_QUEEN * PENALTY_UNPROTECTED_FACTOR
				}
			case 'q':
				blackCount++
				// black queen
				val -= VALUE_QUEEN
				// penalty for moving too early
				if brd.moveNumber < 10 && (rank != 7 || file != 3) {
					val += PENALTY_EARLY_QUEEN_MOVE
				}
				// penalty if left unprotected
				if !IsBlackDefended(brd, rank, file) {
					val += VALUE_QUEEN * PENALTY_UNPROTECTED_FACTOR
				}
			case 'K':
				// penalty if checked
				if IsAttacked(brd, WHITE, rank, file) {
					val -= PENALTY_KING_SAFETY
				}
				// penalty if the king has castled and is still on rank 1 or 2, but the
				// protecting pawns are missing or moved away in the early phase of the game
				if brd.moveNumber < 30 && brd.castlingDone&WHITE_CASTLED == WHITE_CASTLED && rank <= 1 {
					lofile := max(file-1, 0)
					hifile := min(file+1, 7)
					for f := lofile; f <= hifile; f++ {
						if brd.grid[1][f] != 'P' && brd.grid[2][f] != 'P' {
							val -= PENALTY_SHIELD_PAWN_MISSING
						}
					}
				}
			case 'k':
				// penalty if checked
				if IsAttacked(brd, BLACK, rank, file) {
					val += PENALTY_KING_SAFETY
				}
				// penalty if the king has castled and is still on rank 7 or 8, but the
				// protecting pawns are missing or moved away in the early phase of the game
				if brd.moveNumber < 30 && brd.castlingDone&BLACK_CASTLED == BLACK_CASTLED && rank >= 6 {
					lofile := max(file-1, 0)
					hifile := min(file+1, 7)
					for f := lofile; f <= hifile; f++ {
						if brd.grid[5][f] != 'p' && brd.grid[6][f] != 'p' {
							val += PENALTY_SHIELD_PAWN_MISSING
						}
					}
				}
			}
		}
	}

	// penalty for slow development in the first phase of the game
	if brd.moveNumber < 20 {
		for file := 0; file <= 7; file++ {
			if brd.grid[0][file] == 'N' || brd.grid[0][file] == 'B' {
				val -= PENALTY_SLOW_DEVELOPMENT_PER_PIECE
			}
			if brd.grid[7][file] == 'n' || brd.grid[7][file] == 'b' {
				val += PENALTY_SLOW_DEVELOPMENT_PER_PIECE
			}
		}
	}

	// bishop pair bonus
	if whiteBishopCount >= 2 {
		val += BONUS_BISHOP_PAIR
	}
	if blackBishopCount >= 2 {
		val -= BONUS_BISHOP_PAIR
	}

	// castling bonus
	if brd.castlingDone&WHITE_CASTLED == WHITE_CASTLED {
		val += BONUS_CASTLING
	}
	if brd.castlingDone&BLACK_CASTLED == BLACK_CASTLED {
		val -= BONUS_CASTLING
	}

	// penalty if castling not done and castling rights lost
	if !(brd.castlingDone&WHITE_CASTLED == WHITE_CASTLED) && !(brd.castlingRights&WKS == WKS) && !(brd.castlingRights&WQS == WQS) {
		val -= PENALTY_CASTLING_RIGHTS_LOST
	}
	if !(brd.castlingDone&BLACK_CASTLED == BLACK_CASTLED) && !(brd.castlingRights&BKS == BKS) && !(brd.castlingRights&BQS == BQS) {
		val += PENALTY_CASTLING_RIGHTS_LOST
	}

	// Special case: one player has only the king left
	// this happens e.g. in RK vs K end-games.
	// As we cannot solve these in all cases due to limited
	// search depth, we add positional evaluations here.

	if whiteCount+blackCount == 0 {
		// just the two kings left is a draw
		return 0.0
	} else if whiteCount == 0 {
		// lonely white king
		// the closer the white king it is to a border, the better for black
		rw, fw := FindKing(brd, WHITE)
		// penalty for black: 0.0 (if white king at border) ... 30.0 (if in the center)
		val += 10.0 * distanceToBorder(rw, fw)
		// also, black should try to keep their king close to assist in checkmate
		rb, fb := FindKing(brd, BLACK)
		// bonus for black: 0.0 (kings distant) ... 68.0 (kings close)
		val -= 10.0 * (9.90 - distance(rw, fw, rb, fb))

	} else if blackCount == 0 {
		// lonely black king
		// the closer the black king it is to a border, the better for white
		rb, fb := FindKing(brd, BLACK)
		// penalty for white: 0.0 (if black king at border) ... 30.0  (if in the center)
		val -= 10.0 * distanceToBorder(rb, fb)
		// also, white should try to keep their king close to assist in checkmate
		rw, fw := FindKing(brd, WHITE)
		// bonus for white: 0.0 (kings distant) ... 68.0 (kings close)
		val += 10.0 * (9.90 - distance(rw, fw, rb, fb))
	}

	// TODO: add exchange bonus/penalty?
	// TODO: add piece is pinned by check threat penalty?

	return val
}

// compute this:
// 0 0 0 0 0 0 0 0
// 0 1 1 1 1 1 1 0
// 0 1 2 2 2 2 1 0
// 0 1 2 3 3 2 1 0
// 0 1 2 3 3 2 1 0
// 0 1 2 2 2 2 1 0
// 0 1 1 1 1 1 1 0
// 0 0 0 0 0 0 0 0

func distanceToBorder(rank int, file int) float32 {
	if rank > 3 {
		rank = 7 - rank
	}
	if file > 3 {
		file = 7 - file
	}
	dist := rank
	if file < dist {
		dist = file
	}
	return float32(dist)
}

func distance(r1, f1, r2, f2 int) float32 {
	return float32(math.Sqrt(math.Pow(float64(float32(r1-r2)), 2) + math.Pow(float64(float32(f1-f2)), 2)))
}
