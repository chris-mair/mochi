#!/bin/bash

# Solve a few mate-in-4 and mate-in-5 puzzles from the Lichess Open Database
# ( https://database.lichess.org/#puzzles ).
#
# This is useful for benchmarking the search.

function mochi_checkmate {
  PUZZLE_ID=$1
  MATE=$2
  EXPECTED=$3
  FEN=$4

  echo -n "testing Lichess puzzle #$PUZZLE_ID ($MATE): "
  T0=$(python3 -c "import time; print(int(time.time() * 1000))")
  ./mochi checkmate fen "$FEN" | \
  while read LINE; do
    if [[ "$LINE" =~ "$MATE" && "$LINE" =~ "$EXPECTED" ]]; then
      break
    fi
  done
  DURATION=$(python3 -c "import time; print(int(time.time() * 1000) - $T0)")
  printf "%6d ms\n" $DURATION
}


if [ ! -x ./mochi ]; then
  echo "ERROR: missing ./mochi executable - do 'go build' first"
  exit 1
fi

python3 --version > /dev/null
if [ $? != 0 ]; then
  echo "ERROR: missing python3 (just needed for portable timer with ms resolution)"
  exit 1
fi

mochi_checkmate "00LRq" "mate 4" "a6a7" "1k6/1p1q2r1/P2p3p/1NpPpn1Q/5b2/2P5/1P2B1P1/R6K w - - 4 29"
mochi_checkmate "00O9Z" "mate 4" "f6f2" "5Q1R/5p1p/1b3qp1/p6k/P2P4/8/1P2rPPP/5RK1 b - - 8 32"
mochi_checkmate "01FSC" "mate 4" "g4g5" "4r3/5Q1p/p5pk/8/6P1/6KP/P1P1r3/8 w - - 0 35"
mochi_checkmate "0306X" "mate 4" "g2h1" "3r1k2/5ppp/2R5/1B2Q3/8/4BP2/PPP3qP/2K5 b - - 0 23"
mochi_checkmate "01bxA" "mate 4" "g5f7" "5r1k/p5pp/8/6N1/2Q5/8/Pq3P1P/1b3RK1 w - - 4 31"
mochi_checkmate "02owE" "mate 4" "h8h1" "1nkr1qnr/1pp5/p2b1p2/3N2p1/2B1P1p1/4B3/PPPNQPP1/R3R1K1 b - - 1 15"
mochi_checkmate "01sj4" "mate 4" "h4f5" "2r2r2/2q2pk1/5n1p/p1p1pp2/1pb1P2N/3PQ3/PP3PPP/5RK1 w - - 0 23"
mochi_checkmate "03I6j" "mate 4" "d4e2" "r2q3k/4bBpp/p7/1pp2P2/3nn3/2P4Q/PP4PP/R1B2RK1 b - - 0 19"

mochi_checkmate "02iyq" "mate 5" "e5f6" "4r1k1/pp3p1p/q1p2np1/4P1Q1/8/1P1P3P/P3R1P1/7K w - - 0 29"
mochi_checkmate "0HdH1" "mate 5" "g5e3" "5r1k/6p1/2QBB2p/3Pb1q1/4p3/4P1P1/1P5P/2R3K1 b - - 0 35"
mochi_checkmate "051YJ" "mate 5" "h4g5" "7k/pp2q1p1/1n2p1B1/1b3pp1/1n1P3P/8/PP1K1PP1/1Q5R w - - 0 25"
mochi_checkmate "0BjAV" "mate 5" "g3g2" "5R2/P7/8/8/6k1/2b3pp/8/7K b - - 0 52"
mochi_checkmate "0Acau" "mate 5" "b3c2" "8/5p1k/1pQ4p/p2P2p1/Pb1B4/1B3P1P/4n1PK/4q3 w - - 7 35"
mochi_checkmate "0Inzg" "mate 5" "f6g4" "6rk/1Q4pp/5nrq/p4p2/1bNBpP2/4P1PP/PP5K/3R2R1 b - - 15 33"
mochi_checkmate "0AdFZ" "mate 5" "e7g6" "4r2k/1p2Np1p/p1pQ1P2/2P4r/6pq/4R3/P5P1/6K1 w - - 5 39"
mochi_checkmate "0JdqI" "mate 5" "d1h1" "8/4Rp1k/p5pp/3q4/6P1/P3Q2P/5P1K/3r4 b - - 0 41"