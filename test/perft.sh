#!/bin/bash

# Perform a PERFT test for six testing positions from the chess programming wiki
# ( https://www.chessprogramming.org/Perft_Results ).
#
# This is useful to test the correctness of the move generator and for benchmarking.

ERR_CNT=0
MAX_DEPTH=5

function mochi_perft {
  FEN=$1
  EXPECTED=$2
  OUT=$(./mochi perft fen "$FEN" $MAX_DEPTH)
  echo "$OUT"
  MARKER="test perft: PERFT(5) = "
  RESULT="${OUT#*${MARKER}}"
  MARKER="test perft"
  RESULT="${RESULT%?$MARKER*}"
  if [ "$RESULT" == "$EXPECTED" ]; then
    echo "good"
  else
    ERR_CNT=$((ERR_CNT+1))
    echo "bad"
  fi
}

if [ ! -x ./mochi ]; then
  echo "ERROR: missing ./mochi executable - do 'go build' first"
  exit 1
fi

echo "*** Initial Position ***"
mochi_perft "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1" "4865609"

echo "*** Position 2 ***"
mochi_perft "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1" "193690690"

echo "*** Position 3 ***"
mochi_perft "8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 1" "674624"

echo "*** Position 4 ***"
mochi_perft "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1" "15833292"

echo "*** Position 5 ***"
mochi_perft "rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8" "89941194"

echo "*** Position 6 ***"
mochi_perft  "r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10" "164075551"

echo ""
if [ $ERR_CNT -eq 0 ]; then
  echo "-> all tests good"
  exit 0
else
  echo "-> there were errors"
  exit 1
fi