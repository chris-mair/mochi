# MOCHI

MOCHI - the **M**in**O**r **CH**ess eng**I**ne

![MOCHI logo](logo.png)

MOCHI is a toy project to implement a simple chess engine in Go.

MOCHI is an UCI chess _engine_. That means you need another program
if you want to play against it. I've tested it against
[HIARCS Chess Explorer Pro](https://www.hiarcs.com/) (a GUI),
[cutechess-cli](https://github.com/cutechess/cutechess) (a command
line engine test utility) and
the [lichess-bot bridge](https://github.com/lichess-bot-devs/lichess-bot).

You can play against MOCHI on Lichess. Just challenge
[mochi_bot](https://lichess.org/@/mochi_bot)!
It will accept bullet, blitz or rapid challenges with time controls
between 1m+0s and 15m+10s. It will accept rated or unrated challenges
from humans and other bots.

See my blog posts for more information:

- [A chess engine in Go: part 1 - the idea](https://www.1006.org/blog/2022-05-05_a_chess_engine_in_go/)

- [A chess engine in Go: part 2 - board state and FEN input](https://www.1006.org/blog/2022-05-31_chess_engine_mochi/)

- [A chess engine in Go: part 3 - move generator and PERFT results](https://www.1006.org/blog/2022-06-05_chess_perft_results/)

- [A chess engine in Go: part 4 - move generator fixes & logo](https://www.1006.org/blog/2022-06-13_chess_mochi_new_logo/)

- [A chess engine in Go: part 5 - MOCHI plays chess!](https://www.1006.org/blog/2023-05-24_chess_mochi_uci_eval_minimax/)

- [A chess engine in Go: part 6 - play vs. MOCHI on Lichess!](https://www.1006.org/blog/2024-12-05_chess_mochi_improvements_and_lichess/)

## Building

You need the [Go compiler](https://go.dev/) (at least v. 1.21).
To compile MOCHI, just run:

```
go build
```

This will build the executable (`mochi`) for your system. Remember that MOCHI
is just an UCI chess _engine_. You need a chess GUI program to actually play
against it.

## Tests

MOCHI has built-in testing.

### Move Generator Test

To test the **move generator**, run MOCHI with these arguments:

```
./mochi perft fen {fen} {maxDepth}
```

For example, to compute the number of positions reachable after N plies 
(half-moves) from the starting position, up to 5 plies, run:

```
./mochi perft fen "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1" 5
```

Here, the starting position is giving using
[FEN](https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation).
Expected output is:

```
MOCHI v.20250127_1900 by Chris Mair (see LICENSE file)
test perft: PERFT(0) = 1
test perft: PERFT(1) = 20
test perft: PERFT(2) = 400
test perft: PERFT(3) = 8902
test perft: PERFT(4) = 197281
test perft: PERFT(5) = 4865609
test perft: traversed 5072213 nodes in 0.626 s (8105460 nodes/s)
```

There is also a test script that checks some tricky positions
from the [Chess Programming Wiki](https://www.chessprogramming.org/Perft_Results).
Just run:

```
test/perft.sh
```

This will use 1 thread and take about 1 minute on a typical computer.

Expected final line in the output is:

```
-> all tests good
```

### Solving Checkmate Puzzles

To test and benchmark the **search**, MOCHI can be instructed to solve
checkmate puzzles. When run with these arguments:

```
./mochi checkmate fen {fen}
```

MOCHI will analyze the given position until a checkmate is found.

Here is an example showing MOCH find a mate in 4:

```
% ./mochi checkmate fen "1k6/1p1q2r1/P2p3p/1NpPpn1Q/5b2/2P5/1P2B1P1/R6K w - - 4 29" 
MOCHI v.20250306_1100 by Chris Mair (see LICENSE file)
info depth 2 seldepth 6 multipv 1 score cp 17 nodes 1205 nps 264789 pv a6a7
info depth 3 seldepth 7 multipv 1 score cp 37 nodes 2211 nps 243916 pv a6a7
info depth 4 seldepth 8 multipv 1 score cp 22 nodes 18049 nps 398486 pv a6a7
info depth 5 seldepth 9 multipv 1 score cp 37 nodes 98881 nps 973288 pv a6a7
info depth 6 seldepth 10 multipv 1 score cp -32 nodes 533473 nps 750818 pv a6a7
info depth 7 seldepth 11 multipv 1 score mate 4 nodes 244976 nps 520633 pv a6a7
bestmove a6a7
```

There is also a test script that checks this position and some more
mate-in-4 and mate-in-5 puzzles from the Lichess Open Database
( https://database.lichess.org/#puzzles ).

Just run:

```
test/checkmate.sh
```

This will take about 5 minutes on a typical computer.

## Author

Chris Mair <chris@1006.org>

https://www.1006.org


## License

The MOCHI source code is released under the BSD-2-Clause license. See the file LICENSE for details.

## Logo

The MOCHI logo has been designed by Emil B. Mair and is licensed under
CC BY 4.0. See https://creativecommons.org/licenses/by/4.0/ for details.

